
{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    <script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" /> 
   <style type="text/css" >
      
    .ui-combobox-input {
         width:300px;
     }  
     
     .ui-icon { padding: 0; }
     
     #PackagingTypeContainer span { padding: 0; height: 26px; display: block; margin: 0 0 5px 0; vertical-align:bottom;}
     #PackagingTypeContainer input { width: 270px; margin: -16px 10px 0 0; }
     #PackagingTypeContainer img { width: 16px; height: 26px; cursor: pointer;}
     
</style>
 <script type="text/javascript">  
     
$("#AddNewPackagingType").click(function() {
    $('#PackagingTypeContainer').append('<span><input  type="text" class="text" maxlength="30" name="PackageType[]"><img src="{$_subdomain}/images/trash.png" alt="Remove" title="Remove" /></span>');
    $('#PackagingTypeContainer input:last').focus();
    $.colorbox.resize();
});

$("#PackagingTypeContainer").on('click', 'img', function(e) {
    $(this).closest('span').remove();
    e.preventDefault();
});
     
 $(document).ready(function() {
 
        $( "#OrganisationType" ).combobox({ change: function() {  
                                
                                
                                
                                    companyList();

                                
                                                       

                                                    } });
                                                    
                                                    
                function companyList()
                {
                    
                        var $CompanyID = '';

                        if($('#OrganisationType').val()=="Brand")
                        {
                           $('#CompanyIDLabel').html("{$OrganisationTypes.0.Name|escape:'html'}: <sup>*</sup>");   

                           $CompanyID  = $("#BrandID").val();

                        }        
                        else if($('#OrganisationType').val()=="Manufacturer")
                        {
                           $('#CompanyIDLabel').html("{$OrganisationTypes.1.Name|escape:'html'}: <sup>*</sup>");    
                           $CompanyID  = $("#ManufacturerID").val();
                        }
                        else
                        {
                           $('#CompanyIDLabel').html("{$OrganisationTypes.2.Name|escape:'html'}: <sup>*</sup>"); 
                           $CompanyID  = $("#ServiceProviderID").val();
                        }



                        $companyDropDownList = "<option value='' selected='selected' ></option>";
                        $("#CompanyID").html($companyDropDownList);

                        $.post("{$_subdomain}/Data/getOrganisations/"+urlencode($('#OrganisationType').val()),        

                        '',      
                        function(data){
                                var $companyList = eval("(" + data + ")");

                                if($companyList)
                                {


                                    for(var $i=0;$i<$companyList.length;$i++)
                                    {
                                       $checked='';

                                        if($CompanyID==$companyList[$i]['ID'])
                                        {
                                           $checked = ' selected="selected" ';
                                        }    
                                        $companyDropDownList += '<option value="'+$companyList[$i]['ID']+'"  '+$checked+' >'+$companyList[$i]['CompanyName']+'</option>';
                                    }
                                }

                                $("#CompanyID").html($companyDropDownList);
                                //alert($CompanyID);
                                $("#CompanyID").val($CompanyID);
                                setValue("#CompanyID");


                        });
                }
                  
              $("#CompanyID").combobox();    
                  
              companyList();  
              
              
              
 
 {*
    
      $(document).on('change', '#OrganisationType', 
                        function() {

                            var $CompanyID = '';
                            
                            if($('#OrganisationType').val()=="Brand")
                            {
                               $('#CompanyIDLabel').html("{$OrganisationTypes.0.Name|escape:'html'}: <sup>*</sup>");   
                               
                               $CompanyID  = $("#BrandID").val();
                               
                            }        
                            else if($('#OrganisationType').val()=="Manufacturer")
                            {
                               $('#CompanyIDLabel').html("{$OrganisationTypes.1.Name|escape:'html'}: <sup>*</sup>");    
                               $CompanyID  = $("#ManufacturerID").val();
                            }
                            else
                            {
                               $('#CompanyIDLabel').html("{$OrganisationTypes.2.Name|escape:'html'}: <sup>*</sup>"); 
                               $CompanyID  = $("#ServiceProviderID").val();
                            }
                            
                            
                            
                            $companyDropDownList = "<option value='' selected='selected' >{$page['Text']['select_default_option']|escape:'html'}</option>";
                            $("#CompanyID").html($companyDropDownList);
                            
                            $.post("{$_subdomain}/Data/getOrganisations/"+urlencode($('#OrganisationType').val()),        

                            '',      
                            function(data){
                                    var $companyList = eval("(" + data + ")");

                                    if($companyList)
                                    {
                                        

                                        for(var $i=0;$i<$companyList.length;$i++)
                                        {
                                           $checked='';
                           
                                            if($CompanyID==$companyList[$i]['ID'])
                                            {
                                               $checked = ' selected="selected" ';
                                            }    
                                            $companyDropDownList += '<option value="'+$companyList[$i]['ID']+'"  '+$checked+' >'+$companyList[$i]['CompanyName']+'</option>';
                                        }
                                    }

                                    $("#CompanyID").html($companyDropDownList);

                                     



                            });



                        });
                                               
                   *}                            
  
     //$('#OrganisationType').trigger("change");                                               
  
   }); 
   
  </script>  
    
    <div id="CourierFormPanel" class="SystemAdminFormPanel" >
    
                <form id="CourierForm" name="CourierForm" method="post"  action="#" class="inline" >
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

                            </p>
                       
        
                            
                         <p id="OrganisationTypeElement" >
                            <label class="fieldLabel" for="OrganisationType" >{$page['Labels']['organisation_type']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                             
                             <select  name="OrganisationType" id="OrganisationType" title="{$page['Labels']['organisation_type']|escape:'html'}"  class="text" >
                   
                                <option value="" {if $datarow.OrganisationType eq ''}selected="selected"{/if}   >{$page['Text']['select_default_option']|escape:'html'}</option>
                                
                                 {foreach from=$OrganisationTypes item=ot}
                                 <option value="{$ot.Code}" {if $datarow.OrganisationType eq $ot.Code}selected="selected"{/if} >{$ot.Name|escape:'html'}</option>
                                 {/foreach}

                             </select>
                        
                         </p>   
                         
                         
                         
                         <p id="CompanyIDElement" >
                            <label class="fieldLabel" for="CompanyID" id="CompanyIDLabel" ></label>
                           
                             &nbsp;&nbsp; 
                             
                             <select  name="CompanyID" id="CompanyID" class="text" >
                   
                               

                             </select>
                        
                         </p>

			 
			<p>
			    <label class="fieldLabel" for="CourierName" >{$page['Labels']['courier_name']|escape:'html'}:<sup>*</sup></label>
			    &nbsp;&nbsp;
			    <input type="text" class="text" name="CourierName" maxlength="40" value="{$datarow.CourierName|escape:'html'}" id="CourierName" />
			</p>
                       
			 
                         <p>
                            <label class="fieldLabel" for="AccountNo" >{$page['Labels']['account_no']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="AccountNo"  maxlength="40" value="{$datarow.AccountNo|escape:'html'}" id="AccountNo" >
                        
                         </p>
                         
                         
                         <p>
                            <label class="fieldLabel" for="IPAddress" >{$page['Labels']['ip_address']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="IPAddress" maxlength="32" value="{$datarow.IPAddress|escape:'html'}" id="IPAddress" >
                        
                         </p>
                         
                         
                         <p>
                            <label class="fieldLabel" for="UserName" >{$page['Labels']['user_name']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="UserName" maxlength="40" value="{$datarow.UserName|escape:'html'}" id="UserName" >
                        
                         </p>
                         
                         <p>
                            <label class="fieldLabel" for="Password" >{$page['Labels']['password']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="Password" maxlength="40" value="{$datarow.Password|escape:'html'}" id="Password" >
                        
                         </p>
                         
                          <p>
                            <label class="fieldLabel" for="ReportFailureDays" >{$page['Labels']['report_failure_days']|escape:'html'}:</label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text" max="9" min="0" name="ReportFailureDays" value="{$datarow.ReportFailureDays|escape:'html'}" id="ReportFailureDays" >
                        
                         </p>
                         
                         <p>
                            <label class="fieldLabel" for="PackagingType" >{$page['Labels']['packaging_type']|escape:'html'}:</label>
                            <span id="PackagingTypeContainer" style="float: left;margin-left: 10px;">
                                {foreach $package_types as $package_type}
                                <span><input  type="text" class="text" maxlength="30" name="PackageType[]" value="{$package_type|escape:'html'}" ><img src="{$_subdomain}/images/trash.png" alt="Remove" title="Remove" /></span>
                                {foreachelse}
                                <span><input  type="text" class="text" maxlength="30" name="PackageType[]"><img src="{$_subdomain}/images/trash.png" alt="Remove" title="Remove" /></span>
                                {/foreach}
                            </span>
                            <span style="float: right;margin-right: 40px; margin-top: 3px;"><a href="#" id="AddNewPackagingType" title="Add New Packaging Type">Add</a></span>
                           
                         </p>
                         
                         
                          {if $datarow.CourierID neq '' && $datarow.CourierID neq '0'}
                              
                          <p>
                            <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                {foreach $statuses as $status}

                                    <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                                {/foreach}    



                          </p>
                         {/if}
                         

                         <p>
                            <label class="fieldLabel" for="Online" >{$page['Labels']['online']|escape:'html'}:<sup>*</sup></label>
                             &nbsp;&nbsp; 
                             <input  type="radio" name="Online" id="Online" value="Yes" checked="checked" > {$page['Labels']['online_yes']|escape:'html'}
                             <input  type="radio" name="Online" value="No" {if $datarow.Online eq 'No'} checked="checked" {/if} > {$page['Labels']['online_no']|escape:'html'}
                             &nbsp;&nbsp;
                             <input type="checkbox" name="CommunicateIndividualConsignments" {if $datarow.CommunicateIndividualConsignments}checked{/if}>{$page['Labels']['communicate_individual_consignments']|escape:'html'}
                        
                         </p>
                         
			 
			 
                            <p>
                    
                                <span class= "bottomButtons" >

                                    <input type="hidden" name="CourierID"  value="{$datarow.CourierID|escape:'html'}" >
                                   
                                    {if $datarow.CourierID neq '' && $datarow.CourierID neq '0'}
                                        
                                        <input type="submit" name="update_save_btn" class="textSubmitButton" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                    
                                    {else}
                                        
                                        <input type="submit" name="insert_save_btn" class="textSubmitButton" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                        <input type="hidden" name="Status"  value="{$datarow.Status|escape:'html'}" >
                                        
                                    {/if}
                                   
                                        &nbsp;&nbsp;
                                        
                                        <input type="submit" name="cancel_btn" class="textSubmitButton" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >


                                        
                                        <input type="hidden" name="BrandID" id="BrandID"  value="{$datarow.BrandID|escape:'html'}" >
                                        <input type="hidden" name="ManufacturerID" id="ManufacturerID"  value="{$datarow.ManufacturerID|escape:'html'}" >
                                        <input type="hidden" name="ServiceProviderID" id="ServiceProviderID"  value="{$datarow.ServiceProviderID|escape:'html'}" >

                                </span>

                            </p>

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
