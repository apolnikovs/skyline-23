<script type="text/javascript">
    
     var $SkillSetDetails = new Array();
        {foreach from=$Skillset item=ss}

          $SkillSetDetails["{$ss.SkillsetID}"]   =  new Array();   
          $SkillSetDetails["{$ss.SkillsetID}"][0] = "{$ss.ServiceDuration}";
          $SkillSetDetails["{$ss.SkillsetID}"][1]  = "{$ss.EngineersRequired}";

        {/foreach}

    $(document).ready(function() {
        
       
        //Click handler for cancelChanges.
        $(document).on('change', '#SkillsetID', 
                      function() {
                      
                       $("#ServiceDuration").val($SkillSetDetails[$("#SkillsetID").val()][0]);
                       $("#EngineersRequired").val($SkillSetDetails[$("#SkillsetID").val()][1]);
                          
                      });      
    
    });

</script>    

<div id="SPSFormPanel" class="SystemAdminFormPanel" >
    
                <form id="SPSForm" name="SPSForm" method="post"  action="#" class="inline">
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

                            </p>
                       
        
                         <p>
                            <label class="fieldLabel" for="SkillsetID" >{$page['Labels']['skill_name']|escape:'html'}:<sup>*</sup></label>
                           
                            &nbsp;&nbsp;
                            <select name="SkillsetID" id="SkillsetID" >
                               
                                <option value="" {if $datarow.SkillsetID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $Skillset as $ss}

                                    <option value="{$ss.SkillsetID|escape:'html'}" {if $datarow.SkillsetID eq $ss.SkillsetID} selected="selected" {/if}>{$ss.SkillSetName|escape:'html'}</option>

                                {/foreach}

                            </select>
                        
                         </p>
                         
                         
                         <p>
                            <label class="fieldLabel" for="ServiceDuration" >{$page['Labels']['service_interval']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text" style="width:88px;" maxlength="4" name="ServiceDuration" value="{$datarow.ServiceDuration|escape:'html'}" id="ServiceDuration" >
                             &nbsp;&nbsp;{$page['Text']['minutes_required_info']|escape:'html'}
                             
                        
                         </p>
                         
                         
                         <p>
                            <label class="fieldLabel" for="EngineersRequired" >{$page['Labels']['engineers_required']|escape:'html'}:<sup>*</sup></label>
                           
                            &nbsp;&nbsp; <input  type="text" class="text" style="width:88px;" maxlength="2" name="EngineersRequired" value="{$datarow.EngineersRequired|escape:'html'}" id="EngineersRequired" >
                             
                              &nbsp;&nbsp;{$page['Text']['engg_required_info']|escape:'html'}
                             
                         </p>
                         
                         


                          <p>
                            <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                

                             <input  type="radio" name="Status"  value="Active" {if $datarow.Status eq 'Active'} checked="checked" {/if}  /> <span class="text" >{$page['Labels']['active']|escape:'html'}</span> 
                             <input  type="radio" name="Status"  value="In-active" {if $datarow.Status eq 'In-active'} checked="checked" {/if}  /> <span class="text" >{$page['Labels']['inactive']|escape:'html'}</span> 

                               

                          </p>
                         
                         

                            <p>
                    
                                <span class= "bottomButtons" >

                                    <input type="hidden" name="ServiceProviderID"  value="{$datarow.ServiceProviderID|escape:'html'}" >
                                    <input type="hidden" name="ServiceProviderSkillsetID"  value="{$datarow.ServiceProviderSkillsetID|escape:'html'}" >
                                   
                                    {if $datarow.ServiceProviderSkillsetID neq '' && $datarow.ServiceProviderSkillsetID neq '0'}
                                        
                                         <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                    
                                    {else}
                                        
                                        <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                        
                                    {/if}
                                   
                                        <br>
                                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                                </span>

                            </p>

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
 
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
