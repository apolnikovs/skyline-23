<script type="text/javascript">
    $(document).ready(function() {
    
    
         $(document).on('click', '#cancel_email_btn', 
                                function() {
                                
                                
                                    $("#jbContactEmail").val('');
                                    $("#jbNoContactEmail").attr("checked", true);

                                    $(this).colorbox.close(); 
                                    return false;
                                
                                });
    
    
    
    
        $(document).on('click', '#save_email_btn', 
                                function() {

                       
                        
                     $('#noEmailPageForm').validate({

                                ignore: '',

                                rules:  {

                                    popContactEmail:
                                       {
                                           required: true,
                                           email: true
                                       }
                                    },
                                 messages: {

                                    popContactEmail:
                                        {
                                            required: "{$page['Errors']['email']|escape:'html'}",
                                            email: "{$page['Errors']['valid_email']|escape:'html'}"

                                        }
                                    },
                                errorPlacement: function(error, element) {

                                      
                                    error.insertAfter( element );
                                },
                                errorClass: 'fieldError',
                                onkeyup: false,
                                onblur: false,
                                errorElement: 'label',
                                submitHandler: function() {


                                    $("#jbContactEmail").val($("#jbPopContactEmail").val());
                                    $("#jbNoContactEmail").attr("checked", false);

                                    $(this).colorbox.close(); 
                                }    

                         });       

             });
             
           
           
           $(document).on('click', '#all_yours_link', 
                    function() {
                           
                            $("#noEmailPage").fadeOut();
                            $("#allYoursPage").fadeIn("slow");
                         
                            $(this).colorbox.resize({
                             
                               height: ($('#allYoursPage').height()+150)+"px"
                                
                            });
                    }      
                );
             
             
            $(document).on('click', '#close_btn', 
                    function() {
                           
                            $("#allYoursPage").fadeOut();
                            $("#noEmailPage").fadeIn("slow");
                         
                            $(this).colorbox.resize({
                             
                               height: ($('#noEmailPage').height()+150)+"px"
                                
                            });
                            
                         return false;
                    }      
                );  
             
             
             
             
    
      /* =======================================================
                *
                * Initialise input auto-hint functions...
                *
                * ======================================================= */

                $('.auto-hint').focus(function() {
                        $this = $(this);
                        if ($this.val() == $this.attr('title')) {
                            $this.val('').removeClass('auto-hint');
                            if ($this.hasClass('auto-pwd')) {
                                $this.prop('type','password');
                            }
                        }
                    } ).blur(function() {
                        $this = $(this);
                        if ($this.val() == '' && $this.attr('title') != '')  {
                            $this.val($this.attr('title')).addClass('auto-hint');
                            if ($this.hasClass('auto-pwd')) {
                                $this.prop('type','text');
                            }
                        }         
                    } ).each(function(){
                        $this = $(this);
                        if ($this.attr('title') == '') { return; }
                        if ($this.val() == '') { 
                            if ($this.attr('type') == 'password') {
                                $this.addClass('auto-pwd').prop('type','text');
                            }
                            $this.val($this.attr('title')); 
                        } else { 
                            $this.removeClass('auto-hint'); 
                        }
                        $this.attr('autocomplete','off');
                    } );
       
    });
</script>    

<div class="noEmailPage" id="noEmailPage" >
        
   
        <form id="noEmailPageForm" name="noEmailPageForm" method="post"  action="#" class="inline">
       
           <fieldset>
            <legend title="" > {$page['Text']['legend']|escape:'html'} </legend>
               <p>&nbsp;</p>
               
                 <h3 class="insideText" >{$page['Text']['heading']|escape:'html'}</h3>
                 
                 
                <p class="insideText" >
                {$page['Text']['description_text1_1']|escape:'html'} <a href="#" id="all_yours_link" >{$page['Text']['description_text1_2']|escape:'html'}</a> {$page['Text']['description_text1_3']|escape:'html'}
                </p>
                
                <p>&nbsp;</p>
                
                <p class="insideText" >
                {$page['Text']['description_text2']|escape:'html'}
                </p>
                
                
                 <p class="insideText" >
                    <br> 
                    <label>
                        {$page['Labels']['email']|escape:'html'}
                    </label> 
                    <input  type="text" name="popContactEmail" class="text auto-hint" title="{$page['Text']['email']|escape:'html'}" value="" id="jbPopContactEmail" >
                
                    <input type="submit" name="save_email_btn" class="textSubmitButton" id="save_email_btn"   value="{$page['Buttons']['save_email_address']|escape:'html'}" >    
                </p>

                <p style="padding-left:195px;" >

                <input type="submit" name="cancel_email_btn" class="textSubmitButton" id="cancel_email_btn"    value="{$page['Buttons']['cancel_email_btn']|escape:'html'}" >  

                </p>
                
                <p><hr style="height:1px;margin-top:10px;margin-bottom:10px;"></p>
                
                 <p class="insideText" >
                     {$page['Text']['description_text3']|escape:'html'}
                     
                      <table style="background-color: transparent;width:100%;padding-left:30px;margin-bottom:2px;padding-bottom:2px;" >
                            <tr>
                                <td style="vertical-align:top;width:50%;" >
                                 
                                    <ul style="margin-bottom:0px;padding-bottom:0px;" >
                                        <li>{$page['Text']['new_job_booking']|escape:'html'}</li>
                                        <li>{$page['Text']['appointment_date_time']|escape:'html'}</li>
                                        <li>{$page['Text']['appointment_night_before']|escape:'html'}</li>
                                        <li>{$page['Text']['spares_ordered']|escape:'html'}</li>
                                        <li>{$page['Text']['spares_received']|escape:'html'}</li>
                                        <li>{$page['Text']['second_appointment']}</li>
                                        <li>{$page['Text']['repair_completed']|escape:'html'}</li>
                                    </ul>
                                    
                                </td>
                                
                            </tr>

                       </table>
                 
                 </p>
                 
                 <p class="insideText" >
                 {$page['Text']['bottom_text1']|escape:'html'} <span style="color:red;padding:0px;margin:0px;" >{$page['Text']['bottom_text2']|escape:'html'}</span> {$page['Text']['bottom_text3']|escape:'html'}
                 </p>
                
                 
             </fieldset>
        
        </form>
                               
</div>
                 
                 
                 
                 
               
<div  id="allYoursPage" class="allYoursPage" style="display:none;" >
    
        <form id="allYoursForm" name="allYoursForm" method="post"  action="#" class="inline" >
       
           <fieldset>
            <legend title="" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/all_yours_logo.png" width="150" height="36" ></legend>
            
            <p>&nbsp;</p>
            <p class="insideText">
             {$page['Text']['ay_text1']|escape:'html'}
            </p>
           
            
            <p>&nbsp;</p>
            <p class="insideText">
            {$page['Text']['ay_text2']|escape:'html'}
            </p>
            
            <p>&nbsp;</p>
            <p class="insideText">
            {$page['Text']['ay_text3']|escape:'html'}
            <br>
            <ul style="padding-left:80px;" >
                
                <li>{$page['Text']['ay_text_list_1']|escape:'html'}</li>  
                <li>{$page['Text']['ay_text_list_2']|escape:'html'}</li>    
                <li>{$page['Text']['ay_text_list_3']|escape:'html'}</li>    
                <li>{$page['Text']['ay_text_list_4']|escape:'html'}</li>        
                <li>{$page['Text']['ay_text_list_5']|escape:'html'}</li>
            
            </ul>
            </p>
            
           
            <p class="insideText">
             {$page['Text']['ay_text4']|escape:'html'}
            </p>
            
            
            <p>
                    
                        
                  <input type="submit" name="close_btn" class="textSubmitButton" id="close_btn" style="float:right;"  value="{$page['Buttons']['close']|escape:'html'}" >
                        
                         
                     
            </p>
            
            
            </fieldset>
            
        </form>
   
</div>                  

                    
   
             