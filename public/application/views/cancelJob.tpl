{extends "DemoLayout.tpl"}

{block name=config}
{$Title = "Homepage"}
{$PageId = $HomePage}
{/block}

{block name=afterJqueryUI}
   <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
   <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
   <script type="text/javascript" src="{$_subdomain}/js/jquery_plugins/jquery.textareaCounter.plugin.js"></script>
{/block}

{block name=scripts}

<script type="text/javascript">  
    $(document).ready(function(){
    
    
        var CancelReasonOptions = {  
		"maxCharacterSize": 150,  
		"originalStyle":    "jobBookingTextCount",  
		"warningStyle":	    "warningDisplayInfo",  
		"warningNumber":    20,  
		"displayFormat":    "#input Characters | #left Characters Left | #words Words"
	    };
	    $("#CancelReason").textareaCount(CancelReasonOptions, function() {
                
		if($("#CancelReason").val().length > 150) {
                
		    $("#CancelReason").css("color", "red");
                    
		} else {
                
		    $("#CancelReason").css("color", "inherit");
                    
		}
	    }); 
    
    
        $("#CompletionStatus").combobox();
        
        
        //Click handler for Abort Action button.
        $(document).on('click', '#cancel_btn', 
                      function() {


                           $(location).attr('href', "{$_subdomain}/index/jobupdate/{$JobID}");

                           return false;
                      });
                      
                      
         //Click handler for Cancel Job button.
        $(document).on('click', '#update_save_btn', 
                      function() {
                      
                        
                        if($("label.fieldError:visible").length>0)
                        {

                        }
                        else
                        {
                            $("#cancel_btn").hide();
                            $("#update_save_btn").hide();
                            $("#processDisplayText").show();

                        }
                      
                      

                         $('#CancelJobForm').validate({
                                         
                                         
                                            ignore: '',
                                        
                                            rules:  {
                                            
                                                 CompletionStatus:
                                                    {
                                                        required: true
                                                    },
                                                   CancelReason:
                                                    {
                                                        required: true
                                                    } 
                                             },
                                            messages: {
                                            
                                                CompletionStatus:
                                                        {
                                                            required: "{$page['Errors']['reason']|escape:'html'}"
                                                        },
                                                CancelReason:
                                                        {
                                                            required: "{$page['Errors']['cancel_reason']|escape:'html'}"
                                                        }        
                                            
                                            }, 
                                            errorPlacement: function(error, element) {
                                                
                                                error.insertAfter( element );
                                                
                                                $("#processDisplayText").hide();
                                                $("#update_save_btn").show();
                                                $("#cancel_btn").show();
                                                
                                            },
                                            errorClass: 'fieldError',
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: 'label',

                                            submitHandler: function() {
                                            
                                            
                                                $.post("{$_subdomain}/index/canceljob/",         
                                                 $('#CancelJobForm').serializeArray(),      
                                                 function(data){
                                                         var $canceljob  = eval("(" + data + ")");

                                                         if($canceljob['status']=='SUCCESS')
                                                         {
                                                            
                                                            
                                                            $(".bottomPart").hide();
                                                            
                                                            $("#cancelErrorMsg").css('color','#000000').html("{$page['Errors']['canceled']|escape:'html'}").fadeIn('slow');
                                                            
                                                           // location.href = "{$_subdomain}/index/index/";
                                                           
                                                             setTimeout('window.location.href="{$_subdomain}/index/index/"', 6000) /* 6 seconds */
                                                            
                                                             
                                                         }
                                                         else
                                                         {
                                                               $("#processDisplayText").hide();
                                                                $("#update_save_btn").show();
                                                                $("#cancel_btn").show();
                                                              
                                                              $("#cancelErrorMsg").css('color','red').html("{$page['Errors']['not_canceled']|escape:'html'}").fadeIn('slow');

                                                         }



                                                 });
                                                 
                                            }
                           
                      });     
                      
                });         
       
    });

</script>    

{/block}

{block name=body}

<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">{$page['Text']['home_page']|escape:'html'}</a> / <a href="{$_subdomain}/index/jobupdate/{$JobID}">{$page['Text']['job_update']|escape:'html'}</a> / {$page['Text']['cancel_job']|escape:'html'}
    </div>
</div>
    
<div id="menu" >
    
    {if $superadmin}
    
    {menu menu_items = [[$OutstandingJobsPage, 'Add&nbsp;Contact&nbsp;Note','','first',"$('#addContactHistory').trigger('click')"],
                        [$OverdueJobsPage,'Send&nbsp;email',"/index/sendemail/$JobID",''],
                        [$PerformancePage,'Send&nbsp;SMS',"/index/sendsms/$JobID",''],
                        [$LinksPage,'Cancel&nbsp;Job',"/index/canceljob/$JobID",''],
                        [$HomePage,'Other&nbsp;Actions',"/index/otheractions/$JobID",'']]}

    {else}
	
        {foreach $jobMenuItems as $jmkey => $jmvalue}
       
              {$jobMenuItems.$jmkey.2 = $jobMenuItems.$jmkey.2|cat:"/"|cat:$JobID}
              
        {/foreach}
	{menu menu_items = $jobMenuItems}
	
    {/if}
                        
                        
</div> 
                                    
<div class="main" >
                                    
    
    <div   class="jobBookingPanel" >
        <form id="CancelJobForm" name="CancelJobForm" method="post"  action="#" class="inline" autocomplete="off" >
        <fieldset>
            <legend>{$page['Text']['cancel_job']|escape:'html'}</legend>
           
            <p>
                <label></label>
                <span id="cancelErrorMsg"  ></span>
            </p>
            <p class="bottomPart" >
                <label  >{$page['Labels']['reason']|escape:'html'}: <sup>*</sup></label>
                <select name="CompletionStatus" id="CompletionStatus" >
                    
                    
                   <option value="" selected="selected"  ></option>
                    
                   {foreach from=$reasonsArray item=reason}
                        <option value="{$reason|escape:'html'}"  >{$reason|escape:'html'}</option>
                   {/foreach}
                
               </select>
            </p>
            
            
            <p class="bottomPart" >
                <label   >{$page['Labels']['comments']|escape:'html'}: <sup>*</sup></label>
                <textarea style="width:402px;height:75px;"  name="CancelReason" id="CancelReason" ></textarea>
            </p>
            
            <p class="bottomPart" >
                 <label>&nbsp;</label>   
                

                        <input type="hidden" name="JobID"  value="{$JobID|escape:'html'}" >

                   

                        <input type="submit" style="width:96px" name="update_save_btn" class="btnStandard" id="update_save_btn"  value="{$page['Buttons']['cancel_job']|escape:'html'}" >

                        &nbsp;&nbsp;
                        
                        <input type="submit" style="width:96px" name="cancel_btn" class="btnCancel" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['abort_action']|escape:'html'}" >
                            
                        <span id="processDisplayText" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Text']['process_record']|escape:'html'}</span>    
            </p>
            
        </fieldset>
        </form>
    </div>
    <p>&nbsp;</p>
    
    
</div>
{/block}