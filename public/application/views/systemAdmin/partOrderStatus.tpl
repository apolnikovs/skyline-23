{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = "Part Order Status"}
    {$PageId = $PartOrderStatusPage}
    {$fullscreen=true}
{/block}

{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
    <style type="text/css" >
    .ui-combobox-input {
        width:300px;
    }
    </style>
{/block}


{block name=scripts}
<script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/ZeroClipboard.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
    <script type="text/javascript">
    function showTablePreferences(){
$.colorbox({ 
 
                       href:"{$_subdomain}/LookupTables/tableDisplayPreferenceSetup/page=PartOrderStatus/table=PartOrderStatus",
                        title: "Table Display Preferences",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false

                });
}
      
 $(document).ready(function() {

 var oTable = $('#PartOrderStatusResults').dataTable( {
"sDom": '<"left"><"top">t<"bottom"><"centered"><"right">pli<"clear">',
//"sDom": 'Rlfrtip',

"bServerSide": true,

		
    "sAjaxSource": "{$_subdomain}/LookupTables/loadPartOrderStatusTable",
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
				fnCallback(json)
                                $('#tt-Loader').hide();
                                $('#PartOrderStatusResults').show();
                               
			} );
                        },
                        
"bPaginate": false,

"iDisplayLength" : -1,
"aoColumns": [ 
			
			
			{for $er=0 to $data_keys|@count-1}
                                {$vis=1}
                               
                               
                            {if $er==0}{$vis=0}{/if}
                            { "bVisible":{$vis} },
			 
                           {/for} 
                               
                               true
                            
		]
    
   
 
        
          
});//datatable end
  
  
// <div class="bottomButtonsPanel" style="position:absolute;top:-65px;width:200px">
//                            <button type="button" id="edit"  class="gplus-blue">Edit</button>
//                            <button type="button" onclick="PartOrderStatusInsert()" class="gplus-blue">Insert</button>
//                            <button type="button" id="delete" class="gplus-red">Delete</button>
//                    </div>
  
  
   /* Add a click handler to the rows - this could be used as a callback */
	$("#PartOrderStatusResults tbody").click(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                $('#exportType').val(aData[0]);
                }
	});

 /* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}



    /* Add a click handler for the edit row */
	$('button[id^=edit]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
        console.log(anSelected);
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
$.colorbox({ 
 
                           href:"{$_subdomain}/LookupTables/processPartOrderStatus/id="+anSelected[0].id+"/",
                        title: "Edit Part Status",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );                            
             
 /* Add a click handler for the delete row */
	$('button[id^=delete]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
if (confirm('Are you sure you want to delete this entry from database?')) {
    window.location="{$_subdomain}/LookupTables/deletePartOrderStatus/id="+anSelected[0].id
} else {
    // Do nothing!
}


    }else{
    alert("Please select row first");
    }
		
	} );                            
             

/* Add a dblclick handler to the rows - this could be used as a callback */
	$("#PartOrderStatusResults  tbody").dblclick(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
            var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
    
$.colorbox({ 
 
                        href:"{$_subdomain}/LookupTables/processPartOrderStatus/id="+anSelected[0].id+"/",
                        title: "Edit Part Status",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );  



$('input[id^=inactivetick]').click( function() {
      
        if($(this).attr("checked")=="checked"){
                 $('#tt-Loader').show();
                                $('#PartOrderStatusResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadPartOrderStatusTable/inactive=1/");
                 }else{
                  $('#tt-Loader').show();
                                $('#PartOrderStatusResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/LookupTables/loadPartOrderStatusTable/");
                 }
	} );                            


    });//doc ready end
function PartOrderStatusInsert()
{
$.colorbox({ 
 
                        href:"{$_subdomain}/LookupTables/processPartOrderStatus/",
                        title: "Insert PartOrderStatus",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });

}
function PartOrderStatusEdit()
{
    var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); 
               
$.colorbox({ 
 
                        href:"{$_subdomain}/OrganisationSetup/processPartOrderStatus/id="+aData,
                        title: "Edit PartOrderStatus",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });

}


                    
                    
                
                
             
function filterTable(name){
$('input[type="text"]','#PartOrderStatusResults_filter').val(name);
e = jQuery.Event("keyup");
e.which = 13;
$('input[type="text"]','#PartOrderStatusResults_filter').trigger(e);


}

    </script>

    
{/block}


{block name=body}
<!--<div style="float:right">
         <a href="#" onclick="showTablePreferences();">Display Preferences</a>
            </div>-->
    <div class="breadcrumb" style="width:100%">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/lookupTables" >LookupTables</a> {if isset($backtomodel)}/<a href="{$_subdomain}SystemAdmin/index/lookupTables/models" > Models</a> {/if}/ PartOrderStatus

        </div>
    </div>



    <div class="main" id="home" style="width:100%">

               <div class="ServiceAdminTopPanel" >
                    <form id="PartOrderStatusTopForm" name="PartOrderStatusTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >Part Order Status</legend>
                        <p>
                            <label>This status is used on a part to identify the reason why a part order was raised.</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                  <div class="ServiceAdminResultsPanel" id="PartOrderStatusResultsPanel" >
                    
                  
                 
                
                     <div style="text-align:center" id="tt-Loader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
                      
                      
                    <form method="POST" action="{$_subdomain}/LookupTables/saveAssigned" id="PartOrderStatusResultsForm" class="dataTableCorrections">
                        <table style="display:none" id="PartOrderStatusResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                            <thead>
                                    <tr>
                                            
                                        {foreach from=$data_keys key=kk item=vv}
                                  
                                    <th>
                                        {$vv}
                                    </th>
                                  
                                {/foreach}
                                <th style="width:10px"></th>
                                    </tr>
                            </thead>
                            <tbody>
                                
                            
                            </tbody>
                        </table>  
                           
                           
                     </form>
                                 <div style="width:100%;text-align: right">
                        <input id="inactivetick"  type="checkbox" > Show Inactive&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                   
                    </div>
                </div>        

                  <div class="bottomButtonsPanelHolder" style="position: relative;">
                    <div class="bottomButtonsPanel" style="position:absolute;top:-25px;width:200px">
                            <button type="button" id="edit"  class="gplus-blue">Edit</button>
                            <button type="button" onclick="PartOrderStatusInsert()" class="gplus-blue">Insert</button>
                            <button type="button" id="delete" class="gplus-red">Delete</button>
                    </div>
                </div>  
                 <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div>
               

                {if $SupderAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                {/if} 
               


    </div>
                



{/block}



