{extends "DemoLayout.tpl"}


    {block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $Suppliers}
    {$fullscreen=true}
    {/block}
    {block name=afterJqueryUI}
        <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
        <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
        <style type="text/css" >
            .ui-combobox-input {
                 width:300px;
             }  
        </style>
    {/block}
    {block name=scripts}



    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
    <script type="text/javascript" src="{$_subdomain}/js/ColReorder.js"></script>
     <script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
     
    <style>
  .group_move_placeholder {
  display: block;
  height:50px;
  
}
  </style>
   

    <script type="text/javascript">
        
        
  
        
        
        
        var oldValue;
    $(document).ready(function() {
        $("#serviceProviderSelect").combobox({
            change: function() {
                if($(this).val()!="0"){
                    $('#tt-Loader').show();
                    $('#SuppliersResults').hide();
                    oTable.fnReloadAjax("{$_subdomain}/OrganisationSetup/loadSupplierTable/supplierid="+$(this).val()+"/");
                 }else{
                    $('#tt-Loader').show();
                    $('#SuppliersResults').hide();
                    oTable.fnReloadAjax("{$_subdomain}/OrganisationSetup/loadSupplierTable/");
                 }
            }
        });
    var oTable = $('#SuppliersResults').dataTable( {
"sDom": 'R<"left"><"top"f>t<"bottom"><"centered"><"right">pli<"clear">',
//"sDom": 'Rlfrtip',
"bServerSide": true,

		
    "sAjaxSource": "{$_subdomain}/OrganisationSetup/loadSupplierTable/{if $showSpUnmodified|default:''===true}showSpUnmodified=show/{/if}",
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
				fnCallback(json)
                                $('#tt-Loader').hide();
                                $('#SuppliersResults').show();
                               
			} );
                        },
                        "oLanguage": {
                                "sLengthMenu": "_MENU_ Records per page",
                                "sSearch": "Search within results"
                            },
                        
"bPaginate": true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[ 10, 15, 25, 50, 100 , -1], [10, 15, 25, 50, 100, "All"]],
"iDisplayLength" : 10,

"aoColumns": [ 
			
			
			{for $er=0 to $data_keys|@count-1}
                                {$vis=1}
                               
                               
                            {if $er==0}{$vis=0}{/if}
                            { "bVisible":{$vis} },
			 
                           {/for} 
                               
                               { "bVisible":1,"bSortable":false }
                            
		] 
               
   
 
        
          
});//datatable end
  
   /* Add a click handler to the rows - this could be used as a callback */
	$("#SuppliersResults tbody").click(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                $('#exportType').val(aData[0]);
                }
	});
        
      /* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}



    /* Add a click handler for the edit row */
	$('button[id^=edit]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
$.colorbox({ 
 
                        href:"{$_subdomain}/OrganisationSetup/processSupplier/id="+anSelected[0].id,
                        title: "Edit Supplier",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );                            
    
    
    
    
	$('input[id^=inactivetick]').click( function() {
         $('#unaprovedtick').attr("checked",false);
        if($(this).attr("checked")=="checked"){
                 $('#tt-Loader').show();
                                $('#SuppliersResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/OrganisationSetup/loadSupplierTable/inactive=1/");
                 }else{
                  $('#tt-Loader').show();
                                $('#SuppliersResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/OrganisationSetup/loadSupplierTable/");
                 }
	} );  
        var v=0;
	$('a[id^=showpendingtick]').click( function() {
        v++;
        if(v==1){
                 $('#tt-Loader').show();
                                $('#SuppliersResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/OrganisationSetup/loadSupplierTable/pending=1/");
                 }else{
                 v=0;
                  $('#tt-Loader').show();
                                $('#SuppliersResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/OrganisationSetup/loadSupplierTable/");
                 }
	} );                            
    
	$('input[id^=unaprovedtick]').click( function() {
        $('#inactivetick').attr("checked",false);
        if($(this).attr("checked")=="checked"){
                 $('#tt-Loader').show();
                                $('#SuppliersResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/OrganisationSetup/loadSupplierTable/unaproved=1/");
                 }else{
                  $('#tt-Loader').show();
                                $('#SuppliersResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/OrganisationSetup/loadSupplierTable/");
                 }
	} );                            
          
          
          
          
 /* Add a click handler for the delete row */
	$('button[id^=delete]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
if (confirm('Are you sure you want to delete this entry from database?')) {
    window.location="{$_subdomain}/OrganisationSetup/deleteSupplier/id="+anSelected[0].id
} else {
    // Do nothing!
}


    }else{
    alert("Please select row first");
    }
		
	} );                            
             

/* Add a dblclick handler to the rows - this could be used as a callback */
	$("#SuppliersResults  tbody").dblclick(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
            var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
$.colorbox({ 
 
                        href:"{$_subdomain}/OrganisationSetup/processSupplier/id="+anSelected[0].id,
                        title: "Edit Supplier",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );  




	
		
  /*$('#serviceProviderSelect').change( function() {
        
        if($(this).val()!="0"){
                 $('#tt-Loader').show();
                                $('#SuppliersResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/OrganisationSetup/loadSupplierTable/supplierid="+$(this).val()+"/");
                 }else{
                  $('#tt-Loader').show();
                                $('#SuppliersResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/OrganisationSetup/loadSupplierTable/");
                 }
	} );*/       





$('input[id^=showSpUnmodified]').click( function() {
      
     if($(this).attr("checked")=="checked"){
                 $('#tt-Loader').show();
                                $('#SuppliersResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/OrganisationSetup/loadSupplierTable/showSpUnmodified=1/");
                 }else{
                  $('#tt-Loader').show();
                                $('#SuppliersResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/OrganisationSetup/loadSupplierTable/");
                 }
                
                    
		
                 
	} );   


//help icons init
$('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            hide: 'unfocus',
            events: {
         
                hide: function(){
               
                  $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
                }
            },
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                ajax: {
                    url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                    once: false // Re-fetch the content each time I'm shown
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: true // Disable positioning animation
            },
            show: {
                event: 'click',
               
                solo: true // Only show one tooltip at a time
            },
             
            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })
 
    // Make sure it doesn't follow the link when we click it
    .click(function(event) {
        event.preventDefault()
    });
          

} );//doc ready

//displaying table pref colorbox
function showTablePreferences(){
$.colorbox({ 
 
                        href:"{$_subdomain}/OrganisationSetup/tableDisplayPreferenceSetup/page=suppliers/table=supplier",
                        title: "Table Display Preferences",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false

                });
}

function supplierInsert()
{
$.colorbox({ 
 
                        href:"{$_subdomain}/OrganisationSetup/processSupplier/",
                        title: "Insert Supplier",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });

}
function supplierEdit()
{
    var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); 
                alert(aData);
$.colorbox({ 
 
                        href:"{$_subdomain}/OrganisationSetup/processSupplier/id="+aData,
                        title: "Edit Supplier",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });

}

$(document).on('click', '#quick_find_btn', 
                                function() {

                    $.ajax( { type:'POST',
                            dataType:'html',
                            data:'postcode=' + $('#PostalCode').val(),
                            success:function(data, textStatus) { 

                                    if(data=='EM1011' || data.indexOf("EM1011")!=-1)
                                    {

                                            $('#selectOutput').html("<label class='fieldError' >error</label>"); 
                                            $("#selectOutput").slideDown("slow");
                                           

                                            $("#BuildingNameNumber").val('');	
                                            $("#Street").val('');
                                            $("#LocalArea").val('');
                                            $("#TownCity").val('');

                                            $('#PostalCode').focus();
                                    }
                                    else
                                    {
                                        var sow = $('#selectOutput').width();
                                        $('#selectOutput').html(data); 
                                        $("#selectOutput").slideDown("slow");
                                        $('#selectOutput').width(sow);
                                        $("#postSelect").focus();

                                    }




                            },
                            beforeSend:function(XMLHttpRequest){ 
                                   // $('#fetch').fadeIn('medium'); 
                                   $('*').css('cursor','wait');
                            },
                            complete:function(XMLHttpRequest, textStatus) { 
                                   // $('#fetch').fadeOut('medium') 
                                    setTimeout("$('#quickButton').fadeIn('medium')");
                                    $('*').css('cursor','default');
                            },
                            url:'{$_subdomain}/index/addresslookup' 
                        } ); 
                    return false;
                    
                    
                });   
                
                function addFields(selected) {	
                    var selected_split = selected.split(","); 	


                    $("#BuildingNameNumber").val(selected_split[0]).blur();	
                    $("#Street").val(selected_split[1]).blur();
                    $("#LocalArea").val(selected_split[2]).blur();
                    $("#TownCity").val(selected_split[3]).blur();
                    var spf = $("#SPFormFieldset").width();
                    $("#selectOutput").slideUp("slow"); 
                    $("#SPFormFieldset").width(spf);

                }
                
                
   
    </script>

    {/block}

    {block name=body}
     <div style="float:right">
         <a href="#" onclick="showTablePreferences();">Display Preferences</a>
            </div>
    <div class="breadcrumb" style="width:100%">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/organisationSetup" >{$page['Text']['organisation_setup']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>
           
    <div class="main" id="home" style="width:100%">

               <div class="ServiceAdminTopPanel" >
                    <form id="SuppliersTopForm" name="SuppliersTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  
                      
 <div class="ServiceAdminResultsPanel" id="SuppliersResultsPanel" >
                    

                    
                      {if isset($pendingNo)&&$pendingNo>0}
                        <div style="text-align: center;float:left;background:#FFE8F1;border:1px solid red;padding: 4px;">
                            <a id="showpendingtick" href="#">Show Pending</a>
                            <span style="color:red">{$pendingNo}</span> {$page['Labels']['tableName']} Need Approval
                        </div>
                        {/if}
                   
                    
     
                    
                    <form id="SuppliersResultsForm" class="dataTableCorrections">
                        {if isset($splist)}
                        {$page['Labels']['service_provider_label']|escape:'html'}
                        <select id="serviceProviderSelect" style="float:left;margin-left:20px;padding: 4px;position:relative;top:-2px">
                            <option value="0">Please  select Service Provider</option>
                        {foreach $splist as $s}
                            <option value="{$s.ServiceProviderID}">{$s.Acronym}</option>
                        {/foreach}
                        </select>
                        {/if}
                        
                         <div style="float:left;position: relative"><input {if $showSpUnmodified|default:''===true} checked="checked"  {/if}type="checkbox" name="showSpUnmodified" id="showSpUnmodified" > Display Unmodified Records <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerShowUnmodifiedHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" ></div>
                        <table id="SuppliersResults" style="display:none" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
			    <tr>
                                {foreach from=$data_keys key=kk item=vv}
                                  
                                    <th>
                                        {$vv}
                                    </th>
                                  
                                {/foreach}
                                <th style="width:10px"></th>
			    </tr>
                        </thead>
                        <tbody>
                           <div style="text-align:left" id="tt-Loader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
                        </tbody>
                    </table>  
                    <input type="hidden" name="sltSPs" id="sltSPs" >            
                    </form>
                    
                    
                    
                </div>        

                <div class="bottomButtonsPanelHolder" style="position: relative;">
                    <div class="bottomButtonsPanel" style="position:absolute;top:-65px;width:200px">
                            <button type="button" id="edit"  class="gplus-blue">{$page['Buttons']['edit']|escape:'html'}</button>
                            <button type="button" onclick="supplierInsert()" class="gplus-blue">{$page['Buttons']['insert']|escape:'html'}</button>
                            <button type="button" id="delete" class="gplus-red">{$page['Buttons']['delete']|escape:'html'}</button>
                    </div>
                  
                    <div style="width:100%;text-align: right">
                        <input id="inactivetick"  type="checkbox" > Show Inactive&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                     {if $activeUser=="SuperAdmin"} <input id="unaprovedtick"  type="checkbox" > Show Unapproved {/if}
                    </div>
             
                  
                </div>        
                <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div> 

                

                {if $SupderAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                {/if} 
               


    </div>
                <div>
                <hr>
                <button class="gplus-blue" style="float:right" type="button" onclick="window.location='{$_subdomain}/SystemAdmin/index/organisationSetup'">Finish</button>
                </div>
    
                        
                        



{/block}



