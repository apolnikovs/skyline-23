{extends "DemoLayout.tpl"}

{block name=config}
{$Title = $page['Text']['page_title']}
{$PageId = $JobMatchesPage}
{/block}

{block name=scripts}

{*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
{* <link rel="stylesheet" href="{$_subdomain}/css/themes/base/jquery.ui.all.css"> be *}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}


{* <script src="{$_subdomain}/js/ui/jquery.ui.core.js"></script>
<script src="{$_subdomain}/js/ui/jquery.ui.widget.js"></script>
<script src="{$_subdomain}/js/ui/jquery.ui.datepicker.js"></script>

<script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.datepicker.js"></script> be *}

<script type="text/javascript" charset="utf-8" >
    
function updateFn($sRow){ 
    //document.location = '{$_subdomain}/index/jobupdate/'+ $sRow[0] ;
    window.open("{$_subdomain}/index/jobupdate/"+urlencode($sRow[0]));
}

$(document).ready(function() {

    {if $showResultsTables}
        
        $superFilter = new Object();
        $superFilter.ContactLastName = $('#ContactLastName').val();
        $superFilter.BuildingNameNumber = $('#BuildingNameNumber').val();
        $superFilter.Street = $('#Street').val();
        $superFilter.TownCity = $('#TownCity').val();    
        $superFilter.PostalCode= $('#PostalCode').val();
        $superFilter.ColAddPostcode= $('#ColAddPostcode').val();        
        $superFilter.OlderThan  = $('#OlderThan').val();
        $superFilter.NewerThan   = $('#NewerThan').val();
        //$superFilter.SystemStatus   = $('#SystemStatus').val();
        $superFilter.JobStatus   = $('#JobStatus').val();
        //$superFilter.BrandStatus   = $('#BrandStatus').val();
        $superFilter.NetworkID   = $('#NetworkID').val();
        $superFilter.ClientID   = $('#ClientID').val();
        $superFilter.BranchID   = $('#BranchID').val();
        $superFilter.BookingPerson   = $('#BookingPerson').val();
        $superFilter.ProductNo   = $('#ProductNo').val();
        $superFilter.ManufacturerID   = $('#ManufacturerID').val();
        $superFilter.UnitTypeID   = $('#UnitTypeID').val();
        $superFilter.ModelNumber   = $('#ModelNumber').val();
        $superFilter.SerialNo   = $('#SerialNo').val();
        $superFilter.ServiceProviderID   = $('#ServiceProviderID').val();
        $superFilter.ProductLocation   = $('#ProductLocation').val();
        
        
        $('#matches_table').PCCSDataTable( {
            displayButtons:     'P',
            aoColumns: [ 
                null,             
                null, 
                null, 
                null, 
                null,
                null],
            htmlTableId:        'matches_table',
            viewButtonId:       'job_ViewButton',        
            pickCallbackMethod: 'updateFn',
            dblclickCallbackMethod: 'updateFn', 
            fetchDataUrl:       '{$_subdomain}/Data/jobs/advancedSearch/',
            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
            frmErrorMsgClass: 'webformerror',
            frmErrorElement: 'label',
            frmErrorSugMsgClass: 'suggestwebformerror',
            subdomain: '{$_subdomain}',
            superFilter : $superFilter,
            aaSorting:    [[ 0, "desc" ]]


        });
        
     {/if}   
     
      $("#SystemStatus").attr("disabled", "disabled");
      $("#BrandStatus").attr("disabled", "disabled");
     
      $( "#OlderThan" ).datepicker({
			onSelect: function( selectedDate ) {
				$( "#NewerThan" ).datepicker( "option", "maxDate", selectedDate );
			}
		});
      $( "#NewerThan" ).datepicker({
			onSelect: function( selectedDate ) {
				$( "#OlderThan" ).datepicker( "option", "minDate", selectedDate );
			}
		});
     
      $.validator.addMethod("atLeastOne", function(value, element) {  
                
                var $form = $(element).parents('form');
                return $form.find('.text:filled').length;
                
                }, "{$page['Errors']['atleast_one']|escape:'html'}");
                
      $.validator.addMethod("dateRange", function(value, element) {  
                
                if(Date.parse($("#NewerThan").val()) > Date.parse($("#OlderThan").val()))
                {
                   
                    return false;
                }  
                else
                {
                   return true;
                }

                
                }, "{$page['Errors']['date_range']|escape:'html'}");          

    
     $(document).on('click', '#job_search_btn', 
                function() {
                    $('#jobAdvancedSearchForm').validate({


                                rules:  {
                                            ContactLastName:
                                            {
                                                    atLeastOne: true

                                            },
                                            NewerThan:
                                            {
                                                   dateRange: true
                                            }
                                            
                                            
                                            

                                },
                                messages: {
                                            ContactLastName:
                                            {
                                                    atLeastOne: "{$page['Errors']['atleast_one']|escape:'html'}"
                                            },
                                            NewerThan:
                                            {
                                                    dateRange: "{$page['Errors']['date_range']|escape:'html'}"
                                            }
                                },

                                errorPlacement: function(error, element) {

                                        error.insertBefore( $("#formError") );


                                },
                                errorClass: 'fieldError',
                                onkeyup: false,
                                onblur: false,
                                errorElement: 'label'
                                });




                });
                
                
                
               //change handler for network dropdown box.
                $(document).on('change', '#NetworkID', 
                            function() {
                                 
                                $manufacturerDropDownList = $clientDropDownList = $serviceProviderDropDownList = $userDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';

                                var $NetworkID = $("#NetworkID").val();

                                if($NetworkID && $NetworkID!='')
                                {

                                    //Getting clients for selected network.   
                                    $.post("{$_subdomain}/Data/getClients/"+urlencode($NetworkID),        

                                    '',      
                                    function(data){
                                            var $networkClients = eval("(" + data + ")");

                                            if($networkClients)
                                            {

                                                for(var $i=0;$i<$networkClients.length;$i++)
                                                {

                                                    $clientDropDownList += '<option value="'+$networkClients[$i]['ClientID']+'" >'+$networkClients[$i]['ClientName']+'</option>';
                                                }
                                            }


                                            $("#ClientID").html('');
                                            $("#ClientID").html($clientDropDownList);

                                    });



                                    //Getting manufacturer for selected network.   
                                    $.post("{$_subdomain}/Data/getManufacturers/"+urlencode($NetworkID),        

                                    '',      
                                    function(data){
                                            var $networkManufacturers = eval("(" + data + ")");

                                            if($networkManufacturers)
                                            {

                                                for(var $i=0;$i<$networkManufacturers.length;$i++)
                                                {

                                                    $manufacturerDropDownList += '<option value="'+$networkManufacturers[$i]['ManufacturerID']+'" >'+$networkManufacturers[$i]['ManufacturerName']+'</option>';
                                                }
                                            }

                                                $("#ManufacturerID").html('');

                                            $("#ManufacturerID").html($manufacturerDropDownList);

                                    });
                                    
                                    
                                    
                                    //Getting service providers for selected network.   
                                    $.post("{$_subdomain}/Data/getServiceProviders/"+urlencode($NetworkID),        

                                    '',      
                                    function(data){
                                            var $networkServiceProviders = eval("(" + data + ")");

                                            if($networkServiceProviders)
                                            {

                                                for(var $i=0;$i<$networkServiceProviders.length;$i++)
                                                {

                                                    $serviceProviderDropDownList += '<option value="'+$networkServiceProviders[$i]['ServiceProviderID']+'" >'+$networkServiceProviders[$i]['CompanyName']+'</option>';
                                                }
                                            }

                                            $("#ServiceProviderID").html('');

                                            $("#ServiceProviderID").html($serviceProviderDropDownList);

                                    });
                                    
                                    
                                    //Getting users for selected network.   
                                    $.post("{$_subdomain}/Data/getUsers/"+urlencode($NetworkID),        

                                    '',      
                                    function(data){
                                            var $networkUsers = eval("(" + data + ")");

                                            if($networkUsers)
                                            {

                                                for(var $i=0;$i<$networkUsers.length;$i++)
                                                {

                                                    $userDropDownList += '<option value="'+$networkUsers[$i]['UserID']+'" >'+$networkUsers[$i]['FullName']+'</option>';
                                                }
                                            }

                                            $("#BookingPerson").html('');

                                            $("#BookingPerson").html($userDropDownList);

                                    });



                                }
                                else 
                                {
                                    $("#ClientID").html('');
                                    $("#ManufacturerID").html('');
                                    $("#ServiceProviderID").html('');
                                    $("#BookingPerson").html('');


                                    $("#ClientID").html($clientDropDownList);
                                    $("#ManufacturerID").html($manufacturerDropDownList);
                                    $("#ServiceProviderID").html($serviceProviderDropDownList);
                                    $("#BookingPerson").html($userDropDownList);

                                }


                        });           
                
                
                
                
                //change handler for client dropdown box.
                $(document).on('change', '#ClientID', 
                            function() {
                                 
                                $branchDropDownList = $userDropDownList = $unitTypeDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';

                                var $ClientID  = $("#ClientID").val();
                                var $NetworkID = $("#NetworkID").val();

                                if($ClientID && $ClientID!='')
                                {

                                    //Getting branches for selected client and network.   
                                    $.post("{$_subdomain}/Data/getBranches/"+urlencode($ClientID)+"/"+urlencode($NetworkID),        

                                    '',      
                                    function(data){
                                            var $clientBranches = eval("(" + data + ")");

                                            if($clientBranches)
                                            {

                                                for(var $i=0;$i<$clientBranches.length;$i++)
                                                {

                                                    $branchDropDownList += '<option value="'+$clientBranches[$i]['BranchID']+'" >'+$clientBranches[$i]['BranchName']+'</option>';
                                                }
                                            }


                                            $("#BranchID").html('');
                                            $("#BranchID").html($branchDropDownList);

                                    });
                                    
                                    
                                     //Getting users for selected network and client.   
                                    $.post("{$_subdomain}/Data/getUsers/"+urlencode($NetworkID)+"/"+urlencode($ClientID),        

                                    '',      
                                    function(data){
                                            var $networkClientUsers = eval("(" + data + ")");

                                            if($networkClientUsers)
                                            {

                                                for(var $i=0;$i<$networkClientUsers.length;$i++)
                                                {

                                                    $userDropDownList += '<option value="'+$networkClientUsers[$i]['UserID']+'" >'+$networkClientUsers[$i]['FullName']+'</option>';
                                                }
                                            }

                                            $("#BookingPerson").html('');

                                            $("#BookingPerson").html($userDropDownList);

                                    });
                                    
                                    
                                    
                                    //Getting unit types for selected client.   
                                    $.post("{$_subdomain}/Data/getUnitTypes/"+urlencode($ClientID),        

                                    '',      
                                    function(data){
                                            var $clientUnitTypes = eval("(" + data + ")");

                                            if($clientUnitTypes)
                                            {

                                                for(var $i=0;$i<$clientUnitTypes.length;$i++)
                                                {

                                                    $unitTypeDropDownList += '<option value="'+$clientUnitTypes[$i]['UnitTypeID']+'" >'+$clientUnitTypes[$i]['UnitTypeName']+'</option>';
                                                }
                                            }

                                            $("#UnitTypeID").html('');

                                            $("#UnitTypeID").html($unitTypeDropDownList);

                                    });
                                    

                                }
                                else 
                                {
                                    $("#BranchID").html('');
                                    $("#BookingPerson").html('');
                                    $("#UnitTypeID").html('');
                                 
                                    $("#BranchID").html($branchDropDownList);
                                    $("#BookingPerson").html($userDropDownList);
                                    $("#UnitTypeID").html($unitTypeDropDownList);

                                }


                        });           
                   
                
                
                 //change handler for branch dropdown box.
                $(document).on('change', '#BranchID', 
                            function() {
                                 
                                $userDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';

                                var $ClientID  = $("#ClientID").val();
                                var $NetworkID = $("#NetworkID").val();
                                var $BranchID  = $("#BranchID").val();

                                if($BranchID && $BranchID!='')
                                {

                                     //Getting users for selected network and client.   
                                    $.post("{$_subdomain}/Data/getUsers/"+urlencode($NetworkID)+"/"+urlencode($ClientID)+"/"+urlencode($BranchID),        

                                    '',      
                                    function(data){
                                            var $networkClientBranchUsers = eval("(" + data + ")");

                                            if($networkClientBranchUsers)
                                            {

                                                for(var $i=0;$i<$networkClientBranchUsers.length;$i++)
                                                {

                                                    $userDropDownList += '<option value="'+$networkClientBranchUsers[$i]['UserID']+'" >'+$networkClientBranchUsers[$i]['FullName']+'</option>';
                                                }
                                            }

                                            $("#BookingPerson").html('');

                                            $("#BookingPerson").html($userDropDownList);

                                    });
                                    
                                   
                                }
                                else 
                                {
                                    $("#BookingPerson").html('');
                                 
                                    $("#BookingPerson").html($branchDropDownList);
                                }


                        });           
                   
                
                
     
     
    
});//end document ready function


</script>
    
{/block}

{block name=body}

<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">{$page['Text']['home_page']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}
    </div>
</div>
    
{include file='include/menu.tpl'} 
        
<div class="main" id="multiplematches">           
   <div class="SearchPanel">

            <form id="jobAdvancedSearchForm" name="jobAdvancedSearchForm" method="post"
                  action="{$_subdomain}/index/jobAdvancedSearch" >
                <fieldset>
                <legend>{$page['Text']['page_title']|escape:'html'}</legend>
                {*<div class="serviceInstructionsBar">GP7505</div>*}
                <p class="TopText">{$page['Text']['top_text_note']|escape:'html'}</p>
                <p class="centerBtn" >
                    <label id="formError" ></label>
                </p>
                <div class="span-12 last"  >
                    <p> 
                        
                            <label class="fieldLabel" for="ContactLastName" >{$page['Labels']['customer_surname']|escape:'html'}:</label>
                           
                            <input type="text" name="ContactLastName" id="ContactLastName" 
                            value="{$searchStr.ContactLastName}" class="text" tabIndex="1" />
                    </p>
                    <p> 
                            <label class="fieldLabel" for="BuildingNameNumber" >{$page['Labels']['building_number']|escape:'html'}:</label>
                            
                            <input type="text" name="BuildingNameNumber" id="BuildingNameNumber" 
                            value="{$searchStr.BuildingNameNumber}" class="text" tabIndex="2" />
                    </p>
                    <p>
                        
                         <label class="fieldLabel" for="Street" >{$page['Labels']['street']|escape:'html'}:</label>
                           
                            <input type="text" name="Street" id="Street" 
                            value="{$searchStr.Street}" class="text" tabIndex="3" 
                             />
                    </p>
                    <p> 
                          <label class="fieldLabel" for="TownCity" >{$page['Labels']['town']|escape:'html'}:</label>
                           
                        <input type="text" name="TownCity" id="TownCity" 
                            value="{$searchStr.TownCity}" class="text" tabIndex="4" />
                    </p>                    
                    
                    <p> 
                        <label class="fieldLabel" for="PostalCode" >{$page['Labels']['postal_code']|escape:'html'}:</label>
                            
                        
                        <input type="text" name="PostalCode" id="PostalCode" 
                            value="{$searchStr.PostalCode}" class="text" tabIndex="5" />
                        
                    </p>
                    
                    
                     <p> 
                        <label class="fieldLabel" for="ColAddPostcode" >{$page['Labels']['coladd_postal_code']|escape:'html'}:</label>
                            
                        
                        <input type="text" name="ColAddPostcode" id="ColAddPostcode" 
                            value="{$searchStr.ColAddPostcode}" class="text" tabIndex="5" />
                        
                    </p>
                    
                    
                    <p>
                        <label class="fieldLabel" for="NewerThan" >{$page['Labels']['start_date']|escape:'html'}:</label>
                            
                        
                        <input type="text" name="NewerThan" id="NewerThan" 
                            value="{$searchStr.NewerThan}" class="text" tabIndex="7" 
                             />
                    </p>
                    
                    <p> 
                        <label class="fieldLabel" for="OlderThan" >{$page['Labels']['end_date']|escape:'html'}:</label>
                            
                        
                        <input type="text" name="OlderThan" id="OlderThan" 
                            value="{$searchStr.OlderThan}" class="text" tabIndex="6" 
                            />
                    </p>
                   
                   
                    
                    <p>
                          <label class="fieldLabel" for="SystemStatus" >{$page['Labels']['system_status']|escape:'html'}:</label>
                        
                          <select  name="SystemStatus" id="SystemStatus"   class="text" tabIndex="8"  >
                   
                            <option value="" {if $searchStr.SystemStatus eq ''}selected="selected"{/if}   >{$page['Text']['select_default_option']|escape:'html'}</option>
                         </select> 
                    </p>
                    
                    <p>
                          <label class="fieldLabel" for="JobStatus" >{$page['Labels']['job_status']|escape:'html'}:</label>
                            
                        
                          <select  name="JobStatus" id="JobStatus"   class="text" tabIndex="9" >
                   
                            <option value="" {if $searchStr.JobStatus eq ''}selected="selected"{/if}   >{$page['Text']['select_default_option']|escape:'html'}</option>
                            
                            
                            {foreach from=$jobStatusesList item=jobStatus}
                             <option value="{$jobStatus.StatusID|escape:'html'}" {if $searchStr.JobStatus eq $jobStatus.StatusID}selected="selected"{/if} >{$jobStatus.StatusName|escape:'html'}</option>
                            {/foreach}
                            
                            
                         </select> 
                    </p>
                    <p>
                          <label class="fieldLabel" for="BrandStatus" >{$page['Labels']['brand_status']|escape:'html'}:</label>
                            
                        
                          <select  name="BrandStatus" id="BrandStatus"   class="text" tabIndex="10" >
                   
                            <option value="" {if $searchStr.BrandStatus eq ''}selected="selected"{/if}   >{$page['Text']['select_default_option']|escape:'html'}</option>
                         </select> 
                    </p>
                   
                </div>
                <div class="span-11 last"  >

                   
                    {if $user->NetworkID eq 0}
                    <p>
                          <label class="fieldLabel2" for="NetworkID" >{$page['Labels']['network']|escape:'html'}:</label>
                           
                        
                          <select  name="NetworkID" id="NetworkID"   class="text" tabIndex="11" >
                   
                            <option value="" {if $searchStr.NetworkID eq ''}selected="selected"{/if}   >{$page['Text']['select_default_option']|escape:'html'}</option>
                            
                            {foreach from=$networksList item=network}
                             <option value="{$network.NetworkID|escape:'html'}" {if $searchStr.NetworkID eq $network.NetworkID}selected="selected"{/if} >{$network.CompanyName|escape:'html'}</option>
                            {/foreach}
                            
                         </select> 
                    </p>
                    {else}
                        <input type="hidden" name="NetworkID" id="NetworkID" value="{$user->NetworkID|escape:'html'}" >
                    {/if}
                    
                    
                    {if $user->ClientID neq 0 && $user->NetworkID neq 0}
                        <input type="hidden" name="ClientID" id="ClientID" value="{$user->ClientID|escape:'html'}" >
                    
                    {else}
                        <p>
                            <label class="fieldLabel2" for="ClientID" >{$page['Labels']['client']|escape:'html'}:</label>


                            <select  name="ClientID" id="ClientID"   class="text" tabIndex="12" >

                                <option value="" {if $searchStr.ClientID eq ''}selected="selected"{/if}   >{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach from=$clientsList item=client}
                                <option value="{$client.ClientID|escape:'html'}" {if $searchStr.ClientID eq $client.ClientID}selected="selected"{/if} >{$client.ClientName|escape:'html'}</option>
                                {/foreach}

                            </select> 
                        </p>
                    {/if}
                    
                    
                    
                    
                    {if $user->BranchID neq 0 && $user->ClientID neq 0 && $user->NetworkID neq 0}
                        <input type="hidden" name="BranchID" id="BranchID" value="{$user->BranchID|escape:'html'}" >
                   
                    {else}
                         <p>
                            <label class="fieldLabel2" for="BranchID" >{$page['Labels']['branch']|escape:'html'}:</label>


                            <select  name="BranchID" id="BranchID"   class="text" tabIndex="12" >

                                <option value="" {if $searchStr.BranchID eq ''}selected="selected"{/if}   >{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach from=$branchesList item=branch}
                                <option value="{$branch.BranchID|escape:'html'}" {if $searchStr.BranchID eq $branch.BranchID}selected="selected"{/if} >{$branch.BranchName|escape:'html'}</option>
                                {/foreach}

                            </select> 
                        </p>
                    {/if}
                    
                    
                    <p>
                          <label class="fieldLabel2" for="BookingPerson" >{$page['Labels']['booking_person']|escape:'html'}:</label>
                           
                        
                          <select  name="BookingPerson" id="BookingPerson"   class="text" tabIndex="13" >
                   
                            <option value="" {if $searchStr.BookingPerson eq ''}selected="selected"{/if}   >{$page['Text']['select_default_option']|escape:'html'}</option>
                            
                            {foreach from=$bookingPersonsList item=user}
                             <option value="{$user.UserID|escape:'html'}" {if $searchStr.BookingPerson eq $user.UserID}selected="selected"{/if} >{$user.FullName|escape:'html'}</option>
                            {/foreach}
                            
                            
                         </select> 
                    </p>
                    
                    <p>
                         <label class="fieldLabel2" for="ProductNo" >{$page['Labels']['stock_code']|escape:'html'}:</label>
                            
                         <input type="text" name="ProductNo" id="ProductNo" 
                            value="{$searchStr.ProductNo}" class="text" tabIndex="14" 
                            />
                         
                    </p>
                    
                    <p>
                         <label class="fieldLabel2" for="ManufacturerID" >{$page['Labels']['manufacturer']|escape:'html'}:</label>
                           
                        
                          <select  name="ManufacturerID" id="ManufacturerID"   class="text" tabIndex="15" >
                   
                            <option value="" {if $searchStr.ManufacturerID eq ''}selected="selected"{/if}   >{$page['Text']['select_default_option']|escape:'html'}</option>
                            
                            {foreach from=$manufacturerList item=manufacturer}
                            <option value="{$manufacturer.ManufacturerID}" {if $searchStr.ManufacturerID eq $manufacturer.ManufacturerID}selected="selected"{/if} >{$manufacturer.ManufacturerName|escape:'html'}</option>
                            {/foreach}
                    
                         </select> 
                    </p>
                    
                     <p>
                         <label class="fieldLabel2" for="UnitTypeID" >{$page['Labels']['product_type']|escape:'html'}:</label>
                          
                        
                          <select  name="UnitTypeID" id="UnitTypeID"   class="text" tabIndex="16" >
                   
                            <option value="" {if $searchStr.UnitTypeID eq ''}selected="selected"{/if}   >{$page['Text']['select_default_option']|escape:'html'}</option>
                            {foreach from=$unitTypeList item=unitType}
                            <option value="{$unitType.UnitTypeID}" {if $searchStr.UnitTypeID eq $unitType.UnitTypeID}selected="selected"{/if} >{$unitType.UnitTypeName|escape:'html'}</option>
                            {/foreach}
                            
                         </select> 
                    </p>
                    
                    
                    <p>
                         <label class="fieldLabel2" for="ModelNumber" >{$page['Labels']['model_no']|escape:'html'}:</label>
                           
                        <input type="text" name="ModelNumber" id="ModelNumber" 
                            value="{$searchStr.ModelNumber}" class="text" tabIndex="17" 
                            />
                    </p>
                    
                    <p>
                         <label class="fieldLabel2" for="SerialNo" >{$page['Labels']['serial_no']|escape:'html'}:</label>
                         <input type="text" name="SerialNo" id="SerialNo" 
                            value="{$searchStr.SerialNo}" class="text" tabIndex="18" 
                            />
                    </p>
                    
                     <p>
                         <label class="fieldLabel2" for="ServiceProviderID" >{$page['Labels']['service_centre']|escape:'html'}:</label>
                            
                        
                          <select  name="ServiceProviderID" id="ServiceProviderID"   class="text" tabIndex="19" >
                   
                            <option value="" {if $searchStr.ServiceProviderID eq ''}selected="selected"{/if}   >{$page['Text']['select_default_option']|escape:'html'}</option>
                            {foreach from=$serviceProvidersList item=serviceProvider}
                            <option value="{$serviceProvider.ServiceProviderID}" {if $searchStr.ServiceProviderID eq $serviceProvider.ServiceProviderID}selected="selected"{/if} >{$serviceProvider.CompanyName|escape:'html'}</option>
                            {/foreach}
                         </select> 
                    </p>
                    
                     <p>
                         <label class="fieldLabel2" for="ProductLocation" >{$page['Labels']['unit_location']|escape:'html'}:</label>
                            
                        
                          <select  name="ProductLocation" id="ProductLocation"   class="text" tabIndex="20" >
                   
                            <option value="" {if $searchStr.ProductLocation eq ''}selected="selected"{/if}   >{$page['Text']['select_default_option']|escape:'html'}</option>
                            
                            
                            {foreach from=$productLocations item=productLocation}
                            <option value="{$productLocation.ProductLocation}" {if $searchStr.ProductLocation eq $productLocation.ProductLocation}selected="selected"{/if} >{$productLocation.Name|escape:'html'}</option>
                            {/foreach}
                         </select> 
                    </p>
                    
                   
                </div>         
                
                
                
                <p class="centerBtn" >
              
                        

                        <input type="submit" name="job_search_btn" id="job_search_btn" class="textSubmitButton"  value="{$page['Buttons']['search']|escape:'html'}" >
                </p>
                
                </fieldset>
            </form>
    </div> 
                            
   

   {if $showResultsTables}
    <hr />
    <table id="matches_table" border="0" cellpadding="0" cellspacing="0" class="browse">
            <thead>
                <tr>

                        <th width="10%" >{$page['Text']['skyline_no']|escape:'html'}</th>
                        <th width="20%" >{$page['Text']['service_provider_job_no']|escape:'html'}</th>
                        <th width="15%" >{$page['Text']['surname']|escape:'html'}</th>
                        <th width="10%" >{$page['Text']['post_code']|escape:'html'}</th>
                        <th width="25%" >{$page['Text']['description']|escape:'html'}</th>
                        <th width="20%" >{$page['Text']['status']|escape:'html'}</th>

                </tr>
            </thead>
            <tbody>
                <tr><td colspan="6" style="color:#666;">Loading....</td></tr>    
            </tbody>
    </table>
   {/if}       
            
        
   {include file='include/button_panel.tpl'}                
</div>
{/block}