{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = "Homepage"}
    {$PageId = $LoginPage}
{/block}


{block name=scripts}
    
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
<script type="text/javascript" src="{$_subdomain}/js/jquery.autocomplete.js"></script>
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
<script type="text/javascript">
    
$(document).ready(function(){

    $("#name").blur();
    
    {if $branches neq ''}
        
        $("#branch").hide();
    
    {/if}

    $UserBranchID = '';
    $ToBranchName = '';
    $FromBranchName = '';

    //blur handler for username
    /*$(document).on('blur', '#name', 
            function() {

                $uBDropDownList = '<option value="">{$page['Text']['select_branch']|escape:'html'}</option>';

                var $user_name = $("#name").val();

                if($user_name && $user_name!='')
                    {

                        $.post("{$_subdomain}/Data/getUserBranches/"+urlencode($user_name)+"/",        

                        '',      
                        function(data){
                        
                                var $userBranchesList = eval("(" + data + ")");
                                   

                                if($userBranchesList[0] && $userBranchesList[0]['Status']=="FAIL")
                                {
                                        $('#error').html("{$page['Errors']['username_notexists']}").show('slow');
                                        $("#branch").val("");
                                        $("#branch").hide();
                                }
                                else if($userBranchesList.length>0)
                                {
                                    $("#branch").show();
                                    
                                    for(var $i=0;$i<$userBranchesList.length;$i++)
                                    {
                                            
                                         if($userBranchesList[$i]['BranchID']==$userBranchesList[0]['UserBranchID'])
                                         {
                                             $selectedValue = ' selected="selected" ';    
                                             $UserBranchID = $userBranchesList[0]['UserBranchID'];
                                             $FromBranchName = $userBranchesList[$i]['BranchName'];
                                         }
                                         else
                                         {
                                             $selectedValue = '';
                                         }

                                        $uBDropDownList += '<option value="'+$userBranchesList[$i]['BranchID']+'"  '+$selectedValue+'    >'+$userBranchesList[$i]['BranchName']+'</option>';
                                    }
                                    
                                    $("#branch").html($uBDropDownList);
                                }
                                else
                                {
                                    $("#branch").val("");
                                    $("#branch").hide();
                                }

                                

                        });

                    }


            }); */ 

    $('input').jLabel().attr('autocomplete','off').blur(function() {  $('#error').hide('slow'); } );
    //$('form:first *:input[type!=hidden]:first').focus();
   
    $('#name').focus();
   
    
    /* =======================================================
     *
     * set tab on return for input elements with form submit on auto-submit class...
     *
     * ======================================================= */
     
    $('input[type=text],input[type=password]').keypress( function( e ) {
            if (e.which == 13) {  
                $(this).blur();
                if ($(this).hasClass('auto-submit')) {
                    if (validateForm()) $(this).get(0).form.submit();
                } else {
                    $next = $(this).attr('tabIndex') + 1;
                    $('[tabIndex="'+$next+'"]').focus();
                }
                return false;
            }   
        } );
        

        {* var BranchData = [
            {foreach $branches as $b}
            "{$b.branch}"{if not $b@last},
{/if}
            {/foreach}      
        ]; *}
        
        /* $('#branch').autocomplete( {
            lookup: BranchData
        } ); */
 
        /*$('#branch').autocomplete( {
            source: BranchData
        } ); */

     // $('ui-autocomplete').
     
        /*$('#branch').multiselect( {   
            noneSelectedText: "{$page['Text']['select_branch']|escape:'html'}"
        } );*/
     
     
});

function validateForm() {
    
   
    if ($('#name').val()=='') {        
        $('#error').html("{$page['Errors']['username']}").show('slow');
        $('#name').focus();
        return false;
    }
    if ($('#password').val()=='') {
        $('#error').html("{$page['Errors']['password']}").show('slow');
        $('#password').focus();
        return false;
    }
    
    if ($('#branch').is(":visible") && $('#branch').val()=='') {        
        $('#error').html("{$page['Errors']['branch']}").show('slow');
        $('#branch').focus();
        return false;
    }
    
    
     if ($('#branch').is(":visible") && $('#branch').val()!=$UserBranchID) {        
        
        //$('#error').html("{$page['Errors']['branch_changed']}").show('slow');
        
        $ToBranchName  = $('#branch option:selected').text();
        $branch_changed_error = "{$page['Errors']['branch_changed']}";
        $branch_changed_error = $branch_changed_error.replace("%%from_branch_name%%", $FromBranchName); 
        $branch_changed_error = $branch_changed_error.replace("%%to_branch_name%%", $ToBranchName);
        
        return confirm($branch_changed_error);
        
        
        
        $('#branch').focus();
        
        return false;
    }
    
    //$('#name').trigger('blur');
    
    
    if ($('#error').is(":visible"))
    {
         return false;
    }
    
    
    return true;
}

function lostPassword() {
    if ($('#branch').val()=='') {        
        $('#error').html("{$page['Errors']['branch']}").show('slow');
        $('#branch').focus();
        return false;
    }
    if ($('#name').val()=='') {        
        $('#error').html("{$page['Errors']['username']}").show('slow');
        $('#name').focus();
        return false;
    }
    window.location = "{$_subdomain}/Login/lostPassword/"+$('#branch').val()+"/"+$('#name').val();
}
</script>
{/block}

{block name=body}
<div class="main" id="login">
        <form id="loginForm" name="loginForm" method="post" action="{$_subdomain}/Login/index/page=main" class="prepend-5 span-14 last">
            
            <fieldset>
                
                <legend title="">{$page['Text']['legend']|escape:'html'}</legend>
        
               <!-- <p>
                    <span style="display: inline-block; width: 312px; text-align: right; ">
                        <a  href="{$_subdomain}/Login/newUser">{$page['Buttons']['new_user']|escape:'html'}</a>
                    </span>
                </p>-->
                
               <p>&nbsp;</p>
                           
                <p style="text-align:center" >
                    <label for="name">{$page['Labels']['name']}</label>
                    <input type="text" name="name" id="name" value="" class="text jLabelEnabled" tabIndex="2" />
                   <!-- <span class="right-col">
                        <a tabIndex="1" href="{$_subdomain}/Login/lostUsername">{$page['Buttons']['lost_username']|escape:'html'}</a>
                    </span>-->
                </p>
		
                <p style="text-align:center" >
                    <label for="password">{$page['Labels']['password']|escape:'html'}</label>
                    <input type="password" name="password" id="password" value="" class="text jLabelEnabled auto-submit" tabIndex="2" />
                  <!--  <span class="right-col">
                        <a  href="#" onclick="javascript: lostPassword();">{$page['Buttons']['lost_password']|escape:'html'}</a>
                    </span> -->
                </p>
                
                
                <p style="text-align:center" >
                    <select name="branch" id="branch" class="text" tabIndex="3"   >
                        <option value="" >{$page['Text']['select_branch']|escape:'html'}</option>
                        {foreach $branches as $b}
                        <option value="{$b.BranchID}" {if $b.BranchID eq $branch}selected{/if}>{$b.BranchName|escape:'html'}</option>
                        {/foreach}
                    </select>
                    
                    
                    
                </p>
               
                
                
                
                {if $error eq ''}
                <p id="error" class="formError" style="display: none;text-align:center;" />
                {else}
                <p style="text-align:center" id="error" class="formError" >{$error|escape:'html'}</p>
                {/if}

                <p style="text-align:center" >
                    <span style="display: inline-block;text-align: center; ">
                        <a href="javascript:if (validateForm()) document.loginForm.submit();" tabIndex="4" style="margin-right:20px;">{$page['Buttons']['submit']|escape:'html'}</a>
                        
                     
                        
                        <a href="#" onclick="document.location.href='{$_subdomain}/index/index'; return false;"  >{$page['Buttons']['cancel']|escape:'html'}</a>
                    </span>
                   
                </p>
                             
            </fieldset>
            <div class="blankBox" >&nbsp;</div> 
        </form>
              
</div>                      
{/block}
