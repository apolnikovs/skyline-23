<?php

require_once('CustomModel.class.php');


class Guarantee extends CustomModel {
    
    private $conn;
    public  $debug = false;
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   

    /**
    Returns client's bought out guarantee data array
    2012-01-23 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function getBoughtOutGuaranteeData($clientID) {
	
	$q = "	SELECT	    bog.BoughtOutGuaranteeID	AS '0',
	    
			    CASE
				WHEN man.ManufacturerName IS NOT NULL
				THEN man.ManufacturerName
				ELSE 'All Manufacturers'
			    END				AS '1',
			    
			    CASE
				WHEN rs.RepairSkillName IS NOT NULL
				THEN rs.RepairSkillName
				ELSE 'All Repair Skills'
			    END				AS '2',
			    
			    ut.UnitTypeName		AS '3',
			    model.ModelNumber		AS '4',
			    bog.Status			AS '5'
			    
		FROM	    bought_out_guarantee AS bog
		
		LEFT JOIN   manufacturer AS man ON bog.ManufacturerID = man.ManufacturerID
		LEFT JOIN   repair_skill AS rs ON bog.RepairSkillID = rs.RepairSkillID
		LEFT JOIN   unit_type AS ut ON bog.UnitTypeID = ut.UnitTypeID
		LEFT JOIN   model ON bog.ModelID = model.ModelID
		
		WHERE	    bog.ClientID = :clientID
	     ";
	
	$values = ["clientID" => $clientID];
	
	$result = $this->query($this->conn, $q, $values);
	
	return $result;
	
    }
    
    
    
    /**
    Returns repair skills
    2012-01-24 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function getRepairSkills() {
	
	$q = "SELECT * FROM repair_skill order by RepairSkillName";
	$result = $this->query($this->conn, $q);
	return $result;
	
    }
    
    
    
    /**
    Returns Bought Out Guarantee Record for id $bogID
    2012-01-24 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function getRecord($bogID) {
	
	$q = "SELECT * FROM bought_out_guarantee WHERE BoughtOutGuaranteeID = :bogID";
	$values = ["bogID" => $bogID];
	$result = $this->query($this->conn, $q, $values);
	return (isset($result[0])) ? $result[0] : false;
	
    }
    
    
        
    /**
    Save Edit/Insert Bought Out Guarantee record
    2012-01-29 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function saveEditBOG($data) {
	
	if($data["bogID"] != "") {
	
	    $q = "  UPDATE  bought_out_guarantee
		
		    SET	    ManufacturerID = :manufacturerID,
			    RepairSkillID = :repairSkillID,
			    UnitTypeID = :unitTypeID,
			    ModelID = :modelID,
			    Status = :status,
			    ModifiedUserID = :userID,
			    ModifiedDate = now()
		
		    WHERE   BoughtOutGuaranteeID = :bogID
		 ";
	    
	    $values = [	
		"manufacturerID" => ($data["manufacturerID"] ? $data["manufacturerID"] : null),
		"repairSkillID" => ($data["repairSkillID"] ? $data["repairSkillID"] : null),
		"unitTypeID" => ($data["unitTypeID"] ? $data["unitTypeID"] : null),
		"modelID" => ($data["modelID"] ? $data["modelID"] : null),
		"status" => $data["status"],
		"userID" => $this->controller->user->UserID,
		"bogID" => $data["bogID"]
	    ];
	
	    $result = $this->execute($this->conn, $q, $values);
	    
	} else {
	    
	    $q = "  INSERT INTO	bought_out_guarantee				
				(
				    NetworkID,
				    ClientID,
				    ManufacturerID,
				    RepairSkillID,
				    UnitTypeID,
				    ModelID,
				    Status,
				    ModifiedUserID,
				    ModifiedDate
				)
		    VALUES	(
				    :networkID,
				    :clientID,
				    :manufacturerID,
				    :repairSkillID,
				    :unitTypeID,
				    :modelID,
				    :status,
				    :userID,
				    now()
				)
		 ";
	    
	    $values = [
		"networkID" => $data["networkID"],
		"clientID" => $data["clientID"],
		"manufacturerID" => ($data["manufacturerID"] ? $data["manufacturerID"] : null) ,
		"repairSkillID" => ($data["repairSkillID"] ? $data["repairSkillID"] : null),
		"unitTypeID" => ($data["unitTypeID"] ? $data["unitTypeID"] : null),
		"modelID" => ($data["modelID"] ? $data["modelID"] : null),
		"status" => $data["status"],
		"userID" => $this->controller->user->UserID
	    ];
	    
	    $result = $this->execute($this->conn, $q, $values);
	    
	}
	
	
    }
    
     
}
?>
