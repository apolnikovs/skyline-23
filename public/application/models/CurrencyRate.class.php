<?php
/**
 * CurrencyRate.class.php
 * 
 * Routines for interaction with the currency_rate table
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2013 PC Control Systems
 * @link       
 * @version    1.00
 * 
 * Changes
 * Date        Version Author                Reason
 * 12/04/2013  1.00    Andrew J. Williams    Initial Version
 * 29/04/2013  1.01    Andrew J. Williams    Issue 327 - On currency rate table being updated, `ModifiedDate` is not being updated
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

class CurrencyRate extends CustomModel { 
    private $table;                                                             /* For Table Factory Class */
    private $conn;                                                              /* Database Connection */
    
    #public $debug = true;
    
    public function __construct($Controller) {
                  
        parent::__construct($Controller);
        
        $this->conn = $this->Connect(
                                      $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password']
                                    ); 
        
        $this->table = TableFactory::CurrencyRate();
    }
    
    /**
     * UpdateFromEcbXml
     *  
     * Update the rates in the currency table using an XML in a format deom the 
     * European Central Bank
     * 
     * http://www.ecb.int/stats/exchange/eurofxref/html/index.en.html
     * http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml
     * 
     * @param string $xml   Location of ECB xml file
     * 
     * @return      
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function UpdateFromEcbXml($xml = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml") {
        $data = simplexml_load_file($xml); 
        
        $sql = "
                INSERT INTO `currency_rate` SET
                    `Currency` = :currency,
                    `Rate` = :rate,
                    `ModifiedDate` = :updated
                ON DUPLICATE KEY UPDATE 
                    `Rate` = :rate, 
                    `ModifiedDate` = :updated   
               ";
        $qry = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        foreach($data->Cube->Cube->Cube as $rate){   
            echo '&euro;1='.$rate["currency"].' '.$rate["rate"].'<br/>';        /* Output the value of 1EUR for a currency code */
                    
            if($this->debug) $this->controller->log($qry,'currency_');
            $qry->execute(array(':currency' => $rate['currency'], ':rate' => $rate['rate'], ':updated' => date('Y-m-d H:i:s')));            
        } /* next $rate */
    }
    
    /**
     * fetchByName
     *  
     * Return the record associated with a currecy
     * 
     * @param string $cur     Currency Code
     * 
     * @return      Array containing recrd selected 
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function fetchByName($cur) {
        $sql = "
                SELECT 
                        * 
                FROM 
                        `currency_rate`
                WHERE 
                        `Currency` = '$cur'
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]);                                                 /* Currency name found so return record*/
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    /**
     * convert
     *  
     * Convert a value from one currency to another
     * 
     * @param float $value  The value to be converted
     *        string $from  Three letter ISO currency code of current currency
     *        string $to    Three letter ISO currency code of the currecny wanted
     * 
     * @return      Array containing : status => 'SUCCESS' if successful 'FAIL' otherwise
     *                                 message => Description of failure (or if successful X ABC is Y DEF)
     *                                 value => The converted value (if successful)
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function convert($value, $from, $to) {
        $from = strtoupper($from);                                              /* Make sure currency codes are in upper case */
        $to = strtoupper($to);
        
        /* 
         * Stage one
         * Convert from given currency to euro (Database holds all currencies as against Euro - the data does come from the ECB!) 
         */
        
        if ( $from == 'EUR' ) {                                                 /* If the from currency is the Euro */
            $fromrate = 1;                                                      /* We know the rate is 1 as Euros are what we want */
        } else {
            $from_cur_rec = $this->fetchByName($from);                          /* Otherwise look up the currency rate */
            
            if ( is_null($from_cur_rec) ) {                                     /* If no records returned then it is an unknown currency so returnn an error */
                return ( array(
                                'status' => 'FAIL',
                                'message' => "Currency $from unknown"
                       ) );
            } /* fi is_null $from_cur_rec */
            
            $fromrate = 1 / floatval($from_cur_rec['Rate']);                    /* Conversion to Euro is reciprocal of the given rate */
        } /* fi $from == 'EUR' */
        
        $valueeur = $value * $fromrate;                                         /* Value is now converted to Euro */
        
        /* 
         * Stage two
         * Convert from euro to the required currency
         */
        
        if ( $to == 'EUR' ) {                                                   /* If the to currency is the Euro */
            $torate = 1;                                                        /* We know the rate is 1 as Euros are what we want */
        } else {
            $to_cur_rec = $this->fetchByName($to);                              /* Otherwise look up the currency rate */
            
            if ( is_null($to_cur_rec) ) {                                       /* If no records returned then it is an unknown currency so returnn an error */
                return ( array(
                                'status' => 'FAIL',
                                'message' => "Currency $to unknown"
                       ) );
            } /* fi is_null $to_cur_rec */
            
            $torate = floatval($to_cur_rec['Rate']);                            /* Conversion from Euro is the given rate */
        } /* fi $to  == 'EUR' */
        
        $valueto = $valueeur * $torate;                                         /* Value is now converted to required currency */
        
        return ( array(                                                         /* Success so return */
                        'status' => 'SUCCESS',
                        'message' => "$value $from is $valueto $to",
                        'value' => $valueto
               ) );
    }
    
    
    /**
     * getRate
     *  
     * Get the conversion rate between two currencies.
     * 
     * All this actually does is call the conversion method passing 1 as the 
     * required value.
     * 
     * @param string $from  Three letter ISO currency code of current currency
     *        string $to    Three letter ISO currency code of the currency wanted
     * 
     * @return      Array containing : status => 'SUCCESS' if successful 'FAIL' otherwise
     *                                 message => Description of failure (or if successful 1 ABC is X DEF)
     *                                 value => The converted value (if successful)
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getRate($from, $to) {
        return ($this->convert(1, $from, $to));
    }
}
?>