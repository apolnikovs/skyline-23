# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.284');

# ---------------------------------------------------------------------- #
# Add viamente_end_day_history table     								 #
# ---------------------------------------------------------------------- # 
CREATE TABLE IF NOT EXISTS viamente_end_day_history ( ViamenteEndDayHistoryID int(11) NOT NULL AUTO_INCREMENT, ServiceProviderID int(11) DEFAULT NULL, CreatedUserID int(11) DEFAULT NULL, RouteDate date DEFAULT NULL, EndDayType enum('Both','Brown','White') NOT NULL DEFAULT 'Both', CreatedDate timestamp NULL DEFAULT NULL, ActionType enum('Finalise Day','Reset Viamente') DEFAULT NULL, PRIMARY KEY (ViamenteEndDayHistoryID) ) ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Add TAC table     							                    	 #
# ---------------------------------------------------------------------- # 
CREATE TABLE `TAC` (
	`TACID` INT(10) NOT NULL AUTO_INCREMENT,
	`TAC` VARCHAR(15) NOT NULL,
	`ModelID` INT NOT NULL,
	`CreatedDate` TIMESTAMP NULL DEFAULT NULL,
	`CreatedBy` INT NULL,
	`ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`ModifiedBy` INT NULL,
	PRIMARY KEY (`TACID`),
	INDEX `TAC` (`TAC`),
	CONSTRAINT `model_TO_TAC` FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`),
	CONSTRAINT `CreatedBy_user_TO_TAC` FOREIGN KEY (`CreatedBy`) REFERENCES `user` (`UserID`),
	CONSTRAINT `ModifiedBy_user_TO_TAC` FOREIGN KEY (`ModifiedBy`) REFERENCES `user` (`UserID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Add service_provider_TAC table     								     #
# ---------------------------------------------------------------------- # 
CREATE TABLE `service_provider_TAC` (
	`ServiceProviderTACID` INT(10) NOT NULL AUTO_INCREMENT,
	`TAC` VARCHAR(15) NOT NULL,
	`ServiceProviderModelID` INT NOT NULL,
	`CreatedDate` TIMESTAMP NULL DEFAULT NULL,
	`CreatedBy` INT NULL,
	`ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`ModifiedBy` INT NULL,
	PRIMARY KEY (`ServiceProviderTACID`),
	INDEX `TAC` (`TAC`),
	CONSTRAINT `model_TO_service_provider_TAC` FOREIGN KEY (`ServiceProviderModelID`) REFERENCES `service_provider_model` (`ServiceProviderModelID`),
	CONSTRAINT `CreatedBy_user_TO_service_provider_TAC` FOREIGN KEY (`CreatedBy`) REFERENCES `user` (`UserID`),
	CONSTRAINT `ModifiedBy_user_TO_service_provider_TAC` FOREIGN KEY (`ModifiedBy`) REFERENCES `user` (`UserID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.285');
