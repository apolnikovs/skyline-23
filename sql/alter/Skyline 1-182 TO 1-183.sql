# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.182');

# ---------------------------------------------------------------------- #
# Modify table "non_skyline_job"                                                     #
# ---------------------------------------------------------------------- #
ALTER TABLE `non_skyline_job` 
     ADD COLUMN `CustHomeTelNo` VARCHAR(50) NULL AFTER `NetworkRefNo`, 
     ADD COLUMN `CustWorkTelNo` VARCHAR(50) NULL AFTER `CustHomeTelNo`, 
     ADD COLUMN `CustMobileNo` VARCHAR(50) NULL AFTER `CustWorkTelNo`, 
     ADD COLUMN `CollContactNumber` VARCHAR(50) NULL AFTER `CustMobileNo`; 

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.183');
