# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.173');

# ---------------------------------------------------------------------- #
# Modify table "branch"                                                  #
# ---------------------------------------------------------------------- #
ALTER TABLE `branch` ADD COLUMN `ServiceAppraisalRequired` ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER `EndDate`;

 
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.174');