# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-09-18 09:14                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.79');

# ---------------------------------------------------------------------- #
# Add table "Contact_Us_Subject"                                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `Contact_Us_Subject` (
    `ContactUsSubjectID` INTEGER,
    `Subject` VARCHAR(200),
    `PriorityOrder` INTEGER,
    `CreatedDate` DATE,
    `EndDate` DATE,
    `Status` ENUM('Active','In-active') DEFAULT 'Active',
    `ModifiedUserID` INTEGER,
    `ModifiedDate` DATE
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "Contact_Us_Messages"                                        #
# ---------------------------------------------------------------------- #

CREATE TABLE `Contact_Us_Messages` (
    `ContactUsMessageID` INTEGER,
    `CustomerID` INTEGER,
    `ContactUsSubjectID` INTEGER,
    `Message` TEXT,
    `CreatedDate` DATE,
    `EndDate` DATE,
    `Status` ENUM('Active','In-active') DEFAULT 'Active'
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "Welcome_Message_Defaults"                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `Welcome_Message_Defaults` (
    `WelcomeMessageDefaultsID` INTEGER,
    `Title` TEXT,
    `HyperlinkText` VARCHAR(100),
    `Message` TEXT,
    `Type` ENUM('Message','Alert'),
    `CreatedDateTime` DATETIME,
    `ModifiedUserID` INTEGER,
    `ModifiedDate` DATE
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "Welcome_Messages"                                           #
# ---------------------------------------------------------------------- #

CREATE TABLE `Welcome_Messages` (
    `WelcomeMessageID` INTEGER,
    `CustomerID` INTEGER,
    `WelcomeMessageDefaultsID` INTEGER,
    `JobID` INTEGER,
    `CreatedDateTime` DATETIME,
    `EndDate` DATE,
    `Status` ENUM('Active','In-active') DEFAULT 'Active'
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "diary_allocation"                                           #
# ---------------------------------------------------------------------- #

CREATE TABLE `diary_allocation` (
    `DiaryAllocationID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderEngineerSkillsetID` INTEGER,
    `ServiceProviderID` INTEGER,
    `AllocatedDate` DATE,
    `AppointmentAllocationSlotID` INTEGER,
    `TimeFrom` TIME,
    `DiaryPostcodeAllocationID` INTEGER,
    `NumberOfAllocations` INTEGER,
    CONSTRAINT `diary_allocationID` PRIMARY KEY (`DiaryAllocationID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "diary_town_allocation"                                      #
# ---------------------------------------------------------------------- #

CREATE TABLE `diary_town_allocation` (
    `DiaryTownAllocationID` INTEGER NOT NULL AUTO_INCREMENT,
    `DiaryAllocationID` INTEGER,
    `Town` VARCHAR(40),
    CONSTRAINT `diary_town_allocationID` PRIMARY KEY (`DiaryTownAllocationID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "diary_postcode_allocation"                                  #
# ---------------------------------------------------------------------- #

CREATE TABLE `diary_postcode_allocation` (
    `DiaryPostcodeAllocationID` INTEGER NOT NULL AUTO_INCREMENT,
    `DiaryAllocationID` INTEGER,
    `Postcode` VARCHAR(8),
    CONSTRAINT `diary_postcode_allocationID` PRIMARY KEY (`DiaryPostcodeAllocationID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "appointments"                                               #
# ---------------------------------------------------------------------- #

CREATE TABLE `appointments` (
    `AppointmentID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderID` INTEGER,
    `NonSkylineJobID` INTEGER,
    `JobID` INTEGER,
    `NonSkylineJob` ENUM('Yes','No') DEFAULT 'No',
    `DiaryAllocationID` INTEGER,
    `AppointmentDate` DATE,
    `AppointmentStartTime` TIME,
    `AppointmentEndTime` TIME,
    `ServiceProviderEngineerID` INTEGER,
    `OutCardLeft` ENUM('Yes','No') DEFAULT 'No',
    `ServiceBaseAppointmentID` INTEGER,
    `CreatedUserID` INTEGER,
    `AppointmentOrphaned` ENUM('Yes','No') DEFAULT 'No',
    `AppointmentStatusID` INTEGER,
    CONSTRAINT `appointmentsID` PRIMARY KEY (`AppointmentID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "AppointmentSource"                                          #
# ---------------------------------------------------------------------- #

CREATE TABLE `AppointmentSource` (
    `AppointmentSourceID` INTEGER NOT NULL AUTO_INCREMENT,
    `Description` VARCHAR(50),
    CONSTRAINT `AppointmentSourceID` PRIMARY KEY (`AppointmentSourceID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "appointment_status"                                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `appointment_status` (
    `AppointmentStatusID` INTEGER NOT NULL AUTO_INCREMENT,
    `Description` VARCHAR(50),
    CONSTRAINT `appointment_statusID` PRIMARY KEY (`AppointmentStatusID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "service_provider_engineer"                                  #
# ---------------------------------------------------------------------- #

CREATE TABLE `service_provider_engineer` (
    `ServiceProviderEngineerID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderID` INTEGER,
    `EngineerFirstName` VARCHAR(40),
    `EngineerLastName` VARCHAR(40),
    `ServiceBaseUserCode` VARCHAR(4),
    `EmailAddress` VARCHAR(255),
    `RouteColour` VARCHAR(20),
    `StartShift` TIME,
    `EndShift` TIME,
    `LunchBreakDuration` TINYINT(3),
    `LunchPeriodFrom` TIME,
    `LunchPeriodTo` TIME,
    `Status` ENUM('Active','In-active') DEFAULT 'Active',
    `StartPostcode` VARCHAR(8),
    `EndPostcode` VARCHAR(8),
    CONSTRAINT `service_provider_engineerID` PRIMARY KEY (`ServiceProviderEngineerID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "service_provider_engineer_skillset"                         #
# ---------------------------------------------------------------------- #

CREATE TABLE `service_provider_engineer_skillset` (
    `ServiceProviderEngineerSkillsetID` INTEGER NOT NULL AUTO_INCREMENT,
    `EngineerSkillsetID` INTEGER,
    `ServiceDuration` SMALLINT(4),
    CONSTRAINT `service_provider_engineer_skillsetID` PRIMARY KEY (`ServiceProviderEngineerSkillsetID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "engineer_skillset"                                          #
# ---------------------------------------------------------------------- #

CREATE TABLE `engineer_skillset` (
    `EngineerSkillsetID` INTEGER NOT NULL AUTO_INCREMENT,
    `SkillsetName` VARCHAR(40),
    `ServiceDuration` SMALLINT(4),
    CONSTRAINT `engineer_skillsetID` PRIMARY KEY (`EngineerSkillsetID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "appointment_allocation_slot"                                #
# ---------------------------------------------------------------------- #

CREATE TABLE `appointment_allocation_slot` (
    `AppointmentAllocationSlotID` INTEGER NOT NULL AUTO_INCREMENT,
    `Description` VARCHAR(50),
    `TimeFrom` TIME,
    `TimeTo` TIME,
    CONSTRAINT `appointment_allocation_slotID` PRIMARY KEY (`AppointmentAllocationSlotID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "non_skyline_job"                                            #
# ---------------------------------------------------------------------- #

CREATE TABLE `non_skyline_job` (
    `NonSkylineJobID` INTEGER NOT NULL AUTO_INCREMENT,
    `ServiceProviderID` INTEGER,
    `AppointmentID` INTEGER,
    `ServiceProviderJobNo` INTEGER,
    `CustomerSurname` VARCHAR(40),
    `CustomerTitle` VARCHAR(10),
    `Postcode` VARCHAR(8),
    `JobType` INTEGER,
    `JobSourceID` INTEGER,
    CONSTRAINT `non_skyline_jobID` PRIMARY KEY (`NonSkylineJobID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "JobSource"                                                  #
# ---------------------------------------------------------------------- #

CREATE TABLE `JobSource` (
    `JobSourceID` INTEGER NOT NULL AUTO_INCREMENT,
    `Description` VARCHAR(50),
    CONSTRAINT `JobSourceID` PRIMARY KEY (`JobSourceID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.80');
