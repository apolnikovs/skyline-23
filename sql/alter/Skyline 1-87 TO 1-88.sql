# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-10-08 10:35                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.87');

# ---------------------------------------------------------------------- #
# Modify table "skillset"                                                #
# ---------------------------------------------------------------------- #

ALTER TABLE `skillset` DROP COLUMN `JobTypeID`;

ALTER TABLE `skillset` ADD COLUMN `AppointmentTypeID` INTEGER;

CREATE INDEX `IDX_skillset_1` ON `skillset` (`AppointmentTypeID`);

# ---------------------------------------------------------------------- #
# Modify table "non_skyline_job"                                         #
# ---------------------------------------------------------------------- #

ALTER TABLE `non_skyline_job` ADD COLUMN `CustomerAddress1` VARCHAR(50);

ALTER TABLE `non_skyline_job` ADD COLUMN `CustomerAddress2` VARCHAR(50);

ALTER TABLE `non_skyline_job` ADD COLUMN `CustomerAddress3` VARCHAR(50);

ALTER TABLE `non_skyline_job` ADD COLUMN `CustomerAddress4` VARCHAR(50);

ALTER TABLE `non_skyline_job` ADD COLUMN `ProductType` VARCHAR(50);

ALTER TABLE `non_skyline_job` ADD COLUMN `Manufacturer` VARCHAR(50);

ALTER TABLE `non_skyline_job` ADD COLUMN `ModelNumber` VARCHAR(50);

ALTER TABLE `non_skyline_job` ADD COLUMN `Client` VARCHAR(50);

# ---------------------------------------------------------------------- #
# Add table "appintment_type"                                            #
# ---------------------------------------------------------------------- #

CREATE TABLE `appintment_type` (
    `AppointmentTypeID` INTEGER NOT NULL AUTO_INCREMENT,
    `Type` VARCHAR(60),
    `Status` ENUM('Active','In-active') DEFAULT 'Active',
    `CreatedDate` TIMESTAMP,
    `ModifiedUserID` INTEGER,
    `ModefiedDate` TIMESTAMP,
    CONSTRAINT `appintment_typeID` PRIMARY KEY (`AppointmentTypeID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.88');
